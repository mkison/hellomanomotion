﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 ApplicationManager ApplicationManager::get_Instance()
extern void ApplicationManager_get_Instance_mB8F659A6B45A109C0071C1FAE76933A21E0442C9 ();
// 0x00000002 System.Void ApplicationManager::set_Instance(ApplicationManager)
extern void ApplicationManager_set_Instance_m456375F0247E0D44E2AE18F1CC5C9665E4B7DC28 ();
// 0x00000003 System.Void ApplicationManager::Awake()
extern void ApplicationManager_Awake_m9EF712D0014298A38ED83E2400DE69C3D623D7A3 ();
// 0x00000004 System.Void ApplicationManager::Start()
extern void ApplicationManager_Start_m7D8D7EA6EFF8164E3DCEDB45690D575D56314527 ();
// 0x00000005 System.Void ApplicationManager::DisplayInstructions()
extern void ApplicationManager_DisplayInstructions_m7013ECAE6A72367DD985DBF073554CF3777BE895 ();
// 0x00000006 System.Void ApplicationManager::StartMainApplication()
extern void ApplicationManager_StartMainApplication_m8028C0403F4181E9A9092C256C81B2B214DDBB93 ();
// 0x00000007 System.Void ApplicationManager::.ctor()
extern void ApplicationManager__ctor_mC8025C842679B2F85EC46E91B63EB9E2B360F1C9 ();
// 0x00000008 InstructionsManager InstructionsManager::get_Instance()
extern void InstructionsManager_get_Instance_m9D8D043EDC3912EC2F98D81BFCB63BE21E3FA123 ();
// 0x00000009 System.Void InstructionsManager::set_Instance(InstructionsManager)
extern void InstructionsManager_set_Instance_mB4D51BE565A9BD878F12708CF5DC8E8C312D9164 ();
// 0x0000000A System.Void InstructionsManager::Awake()
extern void InstructionsManager_Awake_m082C1F24FAD438E93F01E0FD04E70E7A76CC842D ();
// 0x0000000B System.Void InstructionsManager::IncreaseSlide()
extern void InstructionsManager_IncreaseSlide_mAAA5865E17F2FFCA24B13A870889427BFFBD816B ();
// 0x0000000C System.Void InstructionsManager::DecreaseSlide()
extern void InstructionsManager_DecreaseSlide_m7A4F9FE41B4027E52302A9F64BD3A7717E4022EA ();
// 0x0000000D System.Void InstructionsManager::ShowSlide(System.Int32)
extern void InstructionsManager_ShowSlide_mD63046B258A6409CF1AEAD59BFED86B2B834A784 ();
// 0x0000000E System.Void InstructionsManager::StartInstructions()
extern void InstructionsManager_StartInstructions_m62FF2A3A86B131F420F46901A351F6C9EC0795EA ();
// 0x0000000F System.Void InstructionsManager::ForceInstructions()
extern void InstructionsManager_ForceInstructions_m1488E2B75607F1AC69C073FB233D05BAAC511724 ();
// 0x00000010 System.Void InstructionsManager::UpdateVisualComponents()
extern void InstructionsManager_UpdateVisualComponents_m4154B16FF9DB13F92B6792B35746E12DB1404304 ();
// 0x00000011 System.Void InstructionsManager::HighlightCircles(System.Int32)
extern void InstructionsManager_HighlightCircles_mD854126CE1AC9AF42933BD0899BD4966DD1DCC3B ();
// 0x00000012 System.Void InstructionsManager::SetCircleVisibility(System.Boolean)
extern void InstructionsManager_SetCircleVisibility_m6ED7D7F88686169AC4BAC953092B07E42E67FA8B ();
// 0x00000013 System.Void InstructionsManager::ShowManomotionComponents()
extern void InstructionsManager_ShowManomotionComponents_mEBB1CAA15C84065014091234A9B16C69B1366A1A ();
// 0x00000014 System.Void InstructionsManager::HideManomotionComponents()
extern void InstructionsManager_HideManomotionComponents_m2EB01DB7184F8A538C55A05E69B67D4D22142F79 ();
// 0x00000015 System.Void InstructionsManager::StopShowingInstructions()
extern void InstructionsManager_StopShowingInstructions_m52F4453D6627C61A26F992160CA7DC76922A6BC5 ();
// 0x00000016 System.Void InstructionsManager::DisplayVisualComponents(System.Boolean)
extern void InstructionsManager_DisplayVisualComponents_mDD1B8F44725E5167B3FB50D9205B4F4CDA7EBACF ();
// 0x00000017 System.Void InstructionsManager::GetPreviousManoVisualizationValues()
extern void InstructionsManager_GetPreviousManoVisualizationValues_mB0AD79F161D791E196755C88FEA51C82C2D233AF ();
// 0x00000018 System.Void InstructionsManager::TurnOffManovizualizationValues()
extern void InstructionsManager_TurnOffManovizualizationValues_m07177FF1664DCB846B3692C0B96247857AFADFC5 ();
// 0x00000019 System.Void InstructionsManager::ReInstateVizualizationValues()
extern void InstructionsManager_ReInstateVizualizationValues_m12F0AF964E6761AB0F8E5BA674B1BEC5FA1E070C ();
// 0x0000001A System.Void InstructionsManager::.ctor()
extern void InstructionsManager__ctor_m3DA29F5F41B072CD557D22AA3620855B811957BF ();
// 0x0000001B System.Void SwipeInput::Update()
extern void SwipeInput_Update_m7FDBD7B46AAD627A4CCBC8E3AB5E4F052207C5A3 ();
// 0x0000001C System.Void SwipeInput::DetectSwipe()
extern void SwipeInput_DetectSwipe_m56C6D853C706B5E25734F773713DBA4B69060CBF ();
// 0x0000001D System.Void SwipeInput::EvaluateTouch(UnityEngine.Vector2)
extern void SwipeInput_EvaluateTouch_m3F2FA5ACBC08D242BAAD4C1D94A55812EE7B7B3F ();
// 0x0000001E System.Void SwipeInput::.ctor()
extern void SwipeInput__ctor_m5A1F2147276C5FA9AD5A0F7D3581F5BE24CA05E4 ();
// 0x0000001F System.Void BackgroundModeButtonSelector::Start()
extern void BackgroundModeButtonSelector_Start_m5523414EDBAA77CC11F67C4684C0D49DC44D5401 ();
// 0x00000020 System.Void BackgroundModeButtonSelector::SelectBackgroundMode()
extern void BackgroundModeButtonSelector_SelectBackgroundMode_mD89DD15F1DFCEEE08D412421D8FEE52237704FFA ();
// 0x00000021 System.Void BackgroundModeButtonSelector::.ctor()
extern void BackgroundModeButtonSelector__ctor_m37EDBBF4CF015A46EEBACAB4A600F41594385521 ();
// 0x00000022 System.Void BoundingBoxUI::Start()
extern void BoundingBoxUI_Start_m20B0EF9A6C9B720F67AF71AE57E2ECB7C68E4858 ();
// 0x00000023 System.Void BoundingBoxUI::UpdateInfo(BoundingBox)
extern void BoundingBoxUI_UpdateInfo_m58382D879F88EB5E7FDEF512ABC96FDF20E4C64F ();
// 0x00000024 System.Void BoundingBoxUI::.ctor()
extern void BoundingBoxUI__ctor_mD9728406F85083558C3A83AD3604C6159B8720E8 ();
// 0x00000025 System.Void CalibrationArea::OnMouseOver()
extern void CalibrationArea_OnMouseOver_mEE8609F0B076370A9CAAFC359B938432614DB51A ();
// 0x00000026 System.Void CalibrationArea::OnMouseExit()
extern void CalibrationArea_OnMouseExit_m2FCDD03037F28D7495E296DED3B2D7A468D2CF41 ();
// 0x00000027 System.Void CalibrationArea::.ctor()
extern void CalibrationArea__ctor_m7BA4249E5F23504A41012F48288CD35C2A3F0712 ();
// 0x00000028 System.Void Category::Start()
extern void Category_Start_m0F38089C16A6ACA99234D26FDBCACFD6A2EB0199 ();
// 0x00000029 System.Void Category::InitializeUIValues()
extern void Category_InitializeUIValues_mC6C41DCD2E12844BA7E763556E71AA96D6CB1594 ();
// 0x0000002A System.Void Category::CalculateDimensions()
extern void Category_CalculateDimensions_mFCDAFB9451AB54A10C8532F8C62B31585B2C76A8 ();
// 0x0000002B System.Collections.IEnumerator Category::Calculate()
extern void Category_Calculate_m6D351EA154FA9FB0B68B104014F1418150C8C7C0 ();
// 0x0000002C System.Collections.IEnumerator Category::AlignIcons()
extern void Category_AlignIcons_m5510ACA8D7ADF3CAD15E995F2669A5E253A1B7E0 ();
// 0x0000002D System.Void Category::.ctor()
extern void Category__ctor_mD0975E34D2A70F6F461D35938B64928A717B632B ();
// 0x0000002E CategoryManager CategoryManager::get_Instance()
extern void CategoryManager_get_Instance_mCCC1362A3203C63D8860CFBAAD47A3ECEF5541D9 ();
// 0x0000002F System.Void CategoryManager::set_Instance(CategoryManager)
extern void CategoryManager_set_Instance_m9EF23C5927357F02A9BF24AAA502EB5711674DE7 ();
// 0x00000030 System.Void CategoryManager::Awake()
extern void CategoryManager_Awake_m7CB34D7DC418E1A6CAAD8274F68A47DF441B9780 ();
// 0x00000031 System.Void CategoryManager::Start()
extern void CategoryManager_Start_m303747026E43DE04C5D84351B431E807692FA0F7 ();
// 0x00000032 System.Void CategoryManager::PositionCategories()
extern void CategoryManager_PositionCategories_m7D5CE8A962325C1CAA24AB0DAB3FB326CD0B50CC ();
// 0x00000033 System.Collections.IEnumerator CategoryManager::PositionCategoriesAfter(System.Single)
extern void CategoryManager_PositionCategoriesAfter_mFD96F40B6E8C2216FA3C0F169E9455399E3DE2B0 ();
// 0x00000034 System.Void CategoryManager::.ctor()
extern void CategoryManager__ctor_m403FDA37217ADBF38AEDC15C70AE9B9F09232A30 ();
// 0x00000035 ChooseBackgroundBehavior ChooseBackgroundBehavior::get_Instance()
extern void ChooseBackgroundBehavior_get_Instance_m28E9803E4A6C1766FD6EB3D2D7E30E3DB38F34CA ();
// 0x00000036 System.Void ChooseBackgroundBehavior::Start()
extern void ChooseBackgroundBehavior_Start_m40D84EA16B265D95EA598BB59AD8EA7E02A6D3DA ();
// 0x00000037 System.Void ChooseBackgroundBehavior::CloseAvailableBackgroundMenu()
extern void ChooseBackgroundBehavior_CloseAvailableBackgroundMenu_m8394410C6D62A9CC9F75D7C6173DBB3AEB477956 ();
// 0x00000038 System.Void ChooseBackgroundBehavior::ToggleAvailableBackgroundsMenu()
extern void ChooseBackgroundBehavior_ToggleAvailableBackgroundsMenu_m16F7523808F215291D6D896E7077FD1278FEC29F ();
// 0x00000039 System.Collections.IEnumerator ChooseBackgroundBehavior::CloseAvailableBackgroundMenuAfter()
extern void ChooseBackgroundBehavior_CloseAvailableBackgroundMenuAfter_mACE318610FED9914A59B93380223224CC9483860 ();
// 0x0000003A System.Void ChooseBackgroundBehavior::.ctor()
extern void ChooseBackgroundBehavior__ctor_m157F0277C120464D82ED4312D39EA1A5EC33FADE ();
// 0x0000003B GizmoManager GizmoManager::get_Instance()
extern void GizmoManager_get_Instance_m3F54A2BAEA6C642DFA10798032199510CD75329C ();
// 0x0000003C System.Void GizmoManager::set_Instance(GizmoManager)
extern void GizmoManager_set_Instance_mC96FCB92A4B88053FF84E7594A50219B9F40AC46 ();
// 0x0000003D System.Boolean GizmoManager::get_Show_flags()
extern void GizmoManager_get_Show_flags_m641657D32F5B9AB82EA731F42824057B4C3D955B ();
// 0x0000003E System.Void GizmoManager::set_Show_flags(System.Boolean)
extern void GizmoManager_set_Show_flags_m89F5C774632AFFFADC6EC2976097F7D195D35048 ();
// 0x0000003F System.Boolean GizmoManager::get_Show_hand_states()
extern void GizmoManager_get_Show_hand_states_m0978DA33364266CBEE7081FA3A946CA0BD1B2DD1 ();
// 0x00000040 System.Void GizmoManager::set_Show_hand_states(System.Boolean)
extern void GizmoManager_set_Show_hand_states_mA34EC66F3E7E7C42960EFB2858A2CCA5AB643E5B ();
// 0x00000041 System.Boolean GizmoManager::get_Show_rotation()
extern void GizmoManager_get_Show_rotation_mF8E147BA88A2995F5096E27039E0FC0CD2025D31 ();
// 0x00000042 System.Void GizmoManager::set_Show_rotation(System.Boolean)
extern void GizmoManager_set_Show_rotation_m104D6C640B6964A187B7A0343C4487386194777A ();
// 0x00000043 System.Boolean GizmoManager::get_Show_mano_class()
extern void GizmoManager_get_Show_mano_class_m8CC99CAB42DAB9D058CE823E5D7D6EC3EE8E69CB ();
// 0x00000044 System.Void GizmoManager::set_Show_mano_class(System.Boolean)
extern void GizmoManager_set_Show_mano_class_m8B1F38E2571B84F8341C7FC0853FBB4168F4C1C6 ();
// 0x00000045 System.Boolean GizmoManager::get_Show_trigger_gesture()
extern void GizmoManager_get_Show_trigger_gesture_mD0E38DC389CBCA09078FC499FCF4C82ACBE1EF96 ();
// 0x00000046 System.Void GizmoManager::set_Show_trigger_gesture(System.Boolean)
extern void GizmoManager_set_Show_trigger_gesture_mE90EA80C5810BAA05F48414756A5489AA5243062 ();
// 0x00000047 System.Boolean GizmoManager::get_Show_continuous_gesture()
extern void GizmoManager_get_Show_continuous_gesture_mE9AE6F6D7DD2C2ABAFAAE24E3BF335058FB63334 ();
// 0x00000048 System.Void GizmoManager::set_Show_continuous_gesture(System.Boolean)
extern void GizmoManager_set_Show_continuous_gesture_m26078B571466290E4677573A9B0CA3FDC85B6E91 ();
// 0x00000049 System.Boolean GizmoManager::get_Show_bounding_box()
extern void GizmoManager_get_Show_bounding_box_m11605685960A7F81F83A7277FCA866D8E182DED8 ();
// 0x0000004A System.Void GizmoManager::set_Show_bounding_box(System.Boolean)
extern void GizmoManager_set_Show_bounding_box_m5DE3BBBC5084152FFC6267129C05607691205A63 ();
// 0x0000004B System.Boolean GizmoManager::get_Show_depth()
extern void GizmoManager_get_Show_depth_m225374A4805DAC0476379D8CBF272370F0A39F4C ();
// 0x0000004C System.Void GizmoManager::set_Show_depth(System.Boolean)
extern void GizmoManager_set_Show_depth_m8228D6C89FA67B992A8E8C81998ED49D4CE8F63F ();
// 0x0000004D System.Void GizmoManager::Start()
extern void GizmoManager_Start_m980F0EB0DC1C46B0D6959BD0B0052546AFFCB8C4 ();
// 0x0000004E System.Void GizmoManager::Update()
extern void GizmoManager_Update_m0C72B393569346BBD2E1B751DCA04C2416B75C37 ();
// 0x0000004F System.Void GizmoManager::DisplayFlags(Warning)
extern void GizmoManager_DisplayFlags_m208D15B0F22CBFB1FEE4DC41D0C645CBE570F0AE ();
// 0x00000050 System.Void GizmoManager::DisplayRotationGizmo(TrackingInfo)
extern void GizmoManager_DisplayRotationGizmo_m55EA5B432E0D76C8D018A51047E68A972CE495B2 ();
// 0x00000051 System.Void GizmoManager::DisplayManoclass(GestureInfo)
extern void GizmoManager_DisplayManoclass_m180417D8CFCBEEDF6242882B4054BBB1A49DFD5D ();
// 0x00000052 System.Void GizmoManager::DisplayTriggerGesture(GestureInfo)
extern void GizmoManager_DisplayTriggerGesture_mEB2422AA9F61480FC4F71F164ADF556D1EA05862 ();
// 0x00000053 System.Void GizmoManager::DisplayContinuousGesture(GestureInfo)
extern void GizmoManager_DisplayContinuousGesture_m626CFA07DCE60DA7CA3545CC77847483E01CF56A ();
// 0x00000054 System.Void GizmoManager::DisplayHandState(GestureInfo)
extern void GizmoManager_DisplayHandState_mB974E7A84B9956FBD38A0925B11AAAA27DDFDB84 ();
// 0x00000055 System.Void GizmoManager::DisplayDepth(TrackingInfo)
extern void GizmoManager_DisplayDepth_mFAFF6476877011441D74C23962F884EDCE325CB9 ();
// 0x00000056 System.Void GizmoManager::HighlightStatesToStateDetection(System.Int32)
extern void GizmoManager_HighlightStatesToStateDetection_m06CCE4F7A99F4B4E978B3BD3B6C3535EE4360AA3 ();
// 0x00000057 System.Void GizmoManager::SetRotationGizmoParts()
extern void GizmoManager_SetRotationGizmoParts_mDD3E98D9E0D79CFE44A67ED5221F37AC2A984803 ();
// 0x00000058 System.Void GizmoManager::SetDepthGizmoParts()
extern void GizmoManager_SetDepthGizmoParts_mFE3697AA8645B8AB77BD7FAD2DF81B810DCA8A56 ();
// 0x00000059 System.Void GizmoManager::SetGestureDescriptionParts()
extern void GizmoManager_SetGestureDescriptionParts_mD16BED649CEE58FCEE8376311B4D874D3BBDD38F ();
// 0x0000005A System.Void GizmoManager::.ctor()
extern void GizmoManager__ctor_m04AC7489BBFD5D5E2FBA059B42909D8772620825 ();
// 0x0000005B System.Void HandDetectionModeButtonBehavior::Start()
extern void HandDetectionModeButtonBehavior_Start_m1E576EEB383203280B1C5FC25B7059CC945CBB12 ();
// 0x0000005C System.Void HandDetectionModeButtonBehavior::SelectHandDetectionMode()
extern void HandDetectionModeButtonBehavior_SelectHandDetectionMode_mC03005C5650E26D54090BFA6F5D03315BC2CB924 ();
// 0x0000005D System.Void HandDetectionModeButtonBehavior::.ctor()
extern void HandDetectionModeButtonBehavior__ctor_m7C8CCEC5C3846E46ABE6BC559A0D178170EF844A ();
// 0x0000005E System.Void InteractionArea::Start()
extern void InteractionArea_Start_m04DAFA9F123C6391996F25E92B29EAF527E47C76 ();
// 0x0000005F System.Void InteractionArea::EnterMouse()
extern void InteractionArea_EnterMouse_m664634F32B30EC8631EB19B7E6FA7F11C6B6905C ();
// 0x00000060 System.Void InteractionArea::ExitMouse()
extern void InteractionArea_ExitMouse_m29F93B4F9C3000D8A70657740FF7DDEF1B0BCDC3 ();
// 0x00000061 System.Void InteractionArea::.ctor()
extern void InteractionArea__ctor_mB0DA799B39871257C14FE5E5AB650BCAD454A4D4 ();
// 0x00000062 ManoCalibration ManoCalibration::get_Instance()
extern void ManoCalibration_get_Instance_mA30B72661E2A0336B046455E3519AF4B36D1AD86 ();
// 0x00000063 System.Void ManoCalibration::set_Instance(ManoCalibration)
extern void ManoCalibration_set_Instance_mA6374FA32BA884D5C92C14033A915970AA408834 ();
// 0x00000064 System.Void ManoCalibration::Start()
extern void ManoCalibration_Start_mA8E306E4C4D8600ECB96CB933AF7CC8AF6FBA386 ();
// 0x00000065 System.Void ManoCalibration::Update()
extern void ManoCalibration_Update_m1E279E3B5E106A703CF4935BF4E8196E192ECEC8 ();
// 0x00000066 System.Void ManoCalibration::SaveCalibration()
extern void ManoCalibration_SaveCalibration_m98F8397F9BCD8C780C78C609AA6BCE411E69E573 ();
// 0x00000067 System.Void ManoCalibration::LoadCalibration()
extern void ManoCalibration_LoadCalibration_mA9B8E3701C8B1AA796D3C7692F2DB51EE41AE840 ();
// 0x00000068 System.Void ManoCalibration::DetectCalibration()
extern void ManoCalibration_DetectCalibration_m5BE9C75D3F1B8DAF0C385EE7FB62AD0BD6F1C36B ();
// 0x00000069 System.Collections.IEnumerator ManoCalibration::Calibrate(System.Single)
extern void ManoCalibration_Calibrate_m987B0F97D53CF7A2A27DB21C42FA96EC4C053A8A ();
// 0x0000006A System.Void ManoCalibration::.ctor()
extern void ManoCalibration__ctor_mE430F4F7FDB3093D8B3BFD0DEBBF84564E87782F ();
// 0x0000006B ManoEvents ManoEvents::get_Instance()
extern void ManoEvents_get_Instance_mA0C3DDC467A0A11BEEF0BEE56C1C1F237B73330D ();
// 0x0000006C System.Void ManoEvents::set_Instance(ManoEvents)
extern void ManoEvents_set_Instance_m5164042F083381A1016230F27677092D4491ED7D ();
// 0x0000006D System.Void ManoEvents::add_OnCalibrationSuccess(ManoEvents_ManoEvent)
extern void ManoEvents_add_OnCalibrationSuccess_m9D870E98C348B52863905C3E52A0D692785B270D ();
// 0x0000006E System.Void ManoEvents::remove_OnCalibrationSuccess(ManoEvents_ManoEvent)
extern void ManoEvents_remove_OnCalibrationSuccess_m4171FCDBAAFD8372FE4B73327DB376D4FE5A111F ();
// 0x0000006F System.Void ManoEvents::add_OnCalibrationFailed(ManoEvents_ManoEvent)
extern void ManoEvents_add_OnCalibrationFailed_m296E3BA2759821A6EDDBB0C4AFA5DF5848099973 ();
// 0x00000070 System.Void ManoEvents::remove_OnCalibrationFailed(ManoEvents_ManoEvent)
extern void ManoEvents_remove_OnCalibrationFailed_m08B0FB631121276F33203A9423F09EBEDD37F385 ();
// 0x00000071 System.Void ManoEvents::Awake()
extern void ManoEvents_Awake_mF28C95758639C4C27B5AAC9C9629F4B6A882414F ();
// 0x00000072 System.Void ManoEvents::Update()
extern void ManoEvents_Update_mE3996862A733EFC2F38427968BB7A8CDB337B1BF ();
// 0x00000073 System.Void ManoEvents::HandleManomotionMessages()
extern void ManoEvents_HandleManomotionMessages_m59019849405EE78F761BF9547FF8D5B559CD2161 ();
// 0x00000074 System.Void ManoEvents::DisplayLogMessage(System.String)
extern void ManoEvents_DisplayLogMessage_m33F1F3A00E4F86D8F24AD4CE3C871B2916FE2029 ();
// 0x00000075 System.Void ManoEvents::.ctor()
extern void ManoEvents__ctor_mAED7582697012075494C177D278254F400FCE8AB ();
// 0x00000076 System.Void ManoUtils::add_OnOrientationChanged(ManoUtils_OrientationChange)
extern void ManoUtils_add_OnOrientationChanged_m71F65888A40DAA518ABA5478B09E4248C716731C ();
// 0x00000077 System.Void ManoUtils::remove_OnOrientationChanged(ManoUtils_OrientationChange)
extern void ManoUtils_remove_OnOrientationChanged_mCA61BE79E4126E0147444F23789229548C677086 ();
// 0x00000078 ManoUtils ManoUtils::get_Instance()
extern void ManoUtils_get_Instance_mC1A572CAE9F0FCBA38A805290E16A8C70EEFB82B ();
// 0x00000079 System.Void ManoUtils::Awake()
extern void ManoUtils_Awake_m7EA7EBDB4F6E9A7A18583245D8F9D608311BCC2A ();
// 0x0000007A UnityEngine.Vector3 ManoUtils::CalculateNewPosition(UnityEngine.Vector3,System.Single)
extern void ManoUtils_CalculateNewPosition_m16A3438C80A3BEEC428534A64EC01311F3B928D6 ();
// 0x0000007B System.Void ManoUtils::AjustBorders(UnityEngine.MeshRenderer,Session)
extern void ManoUtils_AjustBorders_mA199A7F01B0BF20F16B34BC2739ED6CE1D148402 ();
// 0x0000007C System.Single ManoUtils::CalculateRatio(UnityEngine.MeshRenderer,Session)
extern void ManoUtils_CalculateRatio_m355AED863DC91E4C4AB07B7015DB5BBA0ACA5434 ();
// 0x0000007D System.Single ManoUtils::CalculateSize(UnityEngine.MeshRenderer,Session,System.Single)
extern void ManoUtils_CalculateSize_m26A93B1043DD4AF3AF969F7CBEAF8F1615B3CF72 ();
// 0x0000007E System.Void ManoUtils::AdjustMeshScale(UnityEngine.MeshRenderer,Session,System.Single,System.Single)
extern void ManoUtils_AdjustMeshScale_m07D04D7B2E309572BC30845DEE37464A6AE1C2EA ();
// 0x0000007F System.Void ManoUtils::CalculateCorrectionPoint(UnityEngine.MeshRenderer,Session,System.Single,System.Single)
extern void ManoUtils_CalculateCorrectionPoint_mB1A40C0EE74C289AC2CB4C991AF164C41BCA5413 ();
// 0x00000080 System.Void ManoUtils::Start()
extern void ManoUtils_Start_m7FFF76891004F54DFF42A191B288A199120D75B0 ();
// 0x00000081 System.Void ManoUtils::Update()
extern void ManoUtils_Update_mF7B5AD9BA65ABEAC0561F658EC352B619742FA3B ();
// 0x00000082 System.Void ManoUtils::CheckForScreenOrientationChange()
extern void ManoUtils_CheckForScreenOrientationChange_m8FC8ED5EEA35AFBD8E2970CBBD3A1E712234A7BD ();
// 0x00000083 UnityEngine.Vector3 ManoUtils::VectorAbs(UnityEngine.Vector3)
extern void ManoUtils_VectorAbs_m768F3210E2F9DDD13376DA3D152797D0CEFB09F3 ();
// 0x00000084 System.Void ManoUtils::OrientMeshRenderer(UnityEngine.MeshRenderer)
extern void ManoUtils_OrientMeshRenderer_m7A106C5DB8100AD01EC53C2AA75091D1BD185B81 ();
// 0x00000085 System.Void ManoUtils::.ctor()
extern void ManoUtils__ctor_m500DC9F1EEFC25C4001EA74297DEC0250CB97A93 ();
// 0x00000086 System.Boolean ManoVisualization::get_Show_inner()
extern void ManoVisualization_get_Show_inner_m564D482056E1A8CF38EB478487AFA01D38D11B73 ();
// 0x00000087 System.Void ManoVisualization::set_Show_inner(System.Boolean)
extern void ManoVisualization_set_Show_inner_m355C719D3C53BCD9659F942C2ED0CFF9FA4505F9 ();
// 0x00000088 System.Boolean ManoVisualization::get_Show_fingertips()
extern void ManoVisualization_get_Show_fingertips_m04DEED3C830B0D2DF8A1D2C2F54EF61FA286DA25 ();
// 0x00000089 System.Void ManoVisualization::set_Show_fingertips(System.Boolean)
extern void ManoVisualization_set_Show_fingertips_m7EDDABC8352F2758E35B9E2987B0C696698DB69B ();
// 0x0000008A System.Boolean ManoVisualization::get_Show_fingertip_labels()
extern void ManoVisualization_get_Show_fingertip_labels_m8CC4611CAB8812CB7EF50A79471DE447BA171532 ();
// 0x0000008B System.Void ManoVisualization::set_Show_fingertip_labels(System.Boolean)
extern void ManoVisualization_set_Show_fingertip_labels_m86C64E2CB33B84E3E9CD1E7059CFA62E3682027D ();
// 0x0000008C System.Boolean ManoVisualization::get_Show_palm_center()
extern void ManoVisualization_get_Show_palm_center_mD4A7255DEA3BACCAC0C49121AF06C7C171EF0317 ();
// 0x0000008D System.Void ManoVisualization::set_Show_palm_center(System.Boolean)
extern void ManoVisualization_set_Show_palm_center_m111ADB07F9B74FA750103642D9A34BBF38FCF1EB ();
// 0x0000008E System.Boolean ManoVisualization::get_Show_contour()
extern void ManoVisualization_get_Show_contour_m3A4FE8BFDC4981C831CF6C19FD8F8D92E3C75902 ();
// 0x0000008F System.Void ManoVisualization::set_Show_contour(System.Boolean)
extern void ManoVisualization_set_Show_contour_m0CB85D6472F71EFA702CF7A6597D2EFF991452EB ();
// 0x00000090 System.Boolean ManoVisualization::get_Show_hand_layer()
extern void ManoVisualization_get_Show_hand_layer_mBC951EA187A7D93588F22CCB792499F2DAB35C55 ();
// 0x00000091 System.Void ManoVisualization::set_Show_hand_layer(System.Boolean)
extern void ManoVisualization_set_Show_hand_layer_m2447158D8D5A3FCF9ED98FEA4002B47778C7CC06 ();
// 0x00000092 System.Boolean ManoVisualization::get_Show_background_layer()
extern void ManoVisualization_get_Show_background_layer_mB02DF8B958BAD185662A1CFE50AFD85CBB222FC3 ();
// 0x00000093 System.Void ManoVisualization::set_Show_background_layer(System.Boolean)
extern void ManoVisualization_set_Show_background_layer_m5FFFD38F5F8F2DD329639B564FDB66273E5A6429 ();
// 0x00000094 System.Boolean ManoVisualization::get_Show_bounding_box()
extern void ManoVisualization_get_Show_bounding_box_m3634CC0161588C79BB3247EEC3169D79D7AA31D8 ();
// 0x00000095 System.Void ManoVisualization::set_Show_bounding_box(System.Boolean)
extern void ManoVisualization_set_Show_bounding_box_m2C7E4AD53CC8879BAA9A7B793DFFF7C27796B168 ();
// 0x00000096 System.Boolean ManoVisualization::get_Show_joints()
extern void ManoVisualization_get_Show_joints_m7B2ADCC5B1E5BF696D26EF9580BC2D58C6BF7692 ();
// 0x00000097 System.Void ManoVisualization::set_Show_joints(System.Boolean)
extern void ManoVisualization_set_Show_joints_mCE65A80054BE0AD0E2E02EB439B0A5C81015F10B ();
// 0x00000098 System.Void ManoVisualization::Start()
extern void ManoVisualization_Start_mD1AA96355E0D2C8DD54DBAA56B3A9258417385E5 ();
// 0x00000099 System.Void ManoVisualization::CreateBoundingBoxes()
extern void ManoVisualization_CreateBoundingBoxes_mE1A7DE649AE5A2125712C5CC6A5551955E12D92D ();
// 0x0000009A System.Void ManoVisualization::SetHandsSupportedByLicence()
extern void ManoVisualization_SetHandsSupportedByLicence_m59CF7E352903AD0E51EEF1DB990AA9BBB1E16520 ();
// 0x0000009B System.Void ManoVisualization::SetPointsBasedOnHandNumber()
extern void ManoVisualization_SetPointsBasedOnHandNumber_mB840EEFC6269CF7C9C96C83BDDC46AEF61249DA9 ();
// 0x0000009C System.Void ManoVisualization::CreatePalmCenterParticle()
extern void ManoVisualization_CreatePalmCenterParticle_mA524EAAED9BC9D562D311F6DDD418EF81E5648BA ();
// 0x0000009D System.Void ManoVisualization::CreateFingerTipParticles()
extern void ManoVisualization_CreateFingerTipParticles_m846F57E24FDEDFBF9DA427D9168F4519E88B3BFC ();
// 0x0000009E System.Void ManoVisualization::CreateFingerTipLabelParticles()
extern void ManoVisualization_CreateFingerTipLabelParticles_m3ED4184DE2706CED33F3BE0C2648C6B2DD316911 ();
// 0x0000009F System.Void ManoVisualization::CreateContourParticles()
extern void ManoVisualization_CreateContourParticles_m4946469CEB7449BF0A30DCAA0FDDF2D3C370185F ();
// 0x000000A0 System.Void ManoVisualization::CreateInnerParticles()
extern void ManoVisualization_CreateInnerParticles_m09A67451FC0F42802D098EBBF6870F986D79BECF ();
// 0x000000A1 System.Void ManoVisualization::Update()
extern void ManoVisualization_Update_m3CDE29CCF897941292C553957760B7DC99EBB856 ();
// 0x000000A2 System.Void ManoVisualization::UpdateLineColor(GestureInfo,System.Int32)
extern void ManoVisualization_UpdateLineColor_mA12AB6455F21839DE3DA32DE57E3B6E7C72A3198 ();
// 0x000000A3 System.Void ManoVisualization::UpdatePalmColor(GestureInfo,System.Int32)
extern void ManoVisualization_UpdatePalmColor_m7CBA39B4F80EDCD0EDCAC196CE8D5F751D0A0F49 ();
// 0x000000A4 System.Void ManoVisualization::ShowPalmCenter(TrackingInfo,System.Int32)
extern void ManoVisualization_ShowPalmCenter_m9865D1BBAC74A7FA059EC9C3E35173B44C12BF89 ();
// 0x000000A5 System.Void ManoVisualization::ShowBoundingBoxInfo(TrackingInfo,System.Int32)
extern void ManoVisualization_ShowBoundingBoxInfo_mBA47D136D3031B8514B3AC9675B606EB3490F024 ();
// 0x000000A6 System.Void ManoVisualization::ShowFingerTips(TrackingInfo,System.Int32)
extern void ManoVisualization_ShowFingerTips_mCFFF62C0F8640B4E8179FB40828848CB55BDC4B5 ();
// 0x000000A7 System.Void ManoVisualization::ShowFingerTipLabels(TrackingInfo,System.Int32)
extern void ManoVisualization_ShowFingerTipLabels_mBB2DC685F1C36B76106E2AD52D28715454E36C9F ();
// 0x000000A8 System.Void ManoVisualization::ShowContour(TrackingInfo,System.Int32)
extern void ManoVisualization_ShowContour_m0388A84E30AFF9F49E835D1BB2CB7319AFE293AD ();
// 0x000000A9 System.Void ManoVisualization::ShowInnerPoints(TrackingInfo,System.Int32)
extern void ManoVisualization_ShowInnerPoints_mA060165F6EE15B6BBDFA71F0A5B811195938715D ();
// 0x000000AA System.Void ManoVisualization::HighLightWithColor(UnityEngine.Color,System.Int32)
extern void ManoVisualization_HighLightWithColor_m5BA73861CEA5A9C6C40FDEC7E4955F2154919025 ();
// 0x000000AB System.Void ManoVisualization::ShowLayering(HandInfoUnity,UnityEngine.MeshRenderer,ManomotionManager)
extern void ManoVisualization_ShowLayering_mAB412E294CCA1006BA0FA00CBC9446F51BE47F35 ();
// 0x000000AC System.Void ManoVisualization::ShowBackground(UnityEngine.Texture2D,UnityEngine.MeshRenderer)
extern void ManoVisualization_ShowBackground_m102F799D27B4E9C02A4C42BAD953FE5B1FC54D24 ();
// 0x000000AD System.Collections.IEnumerator ManoVisualization::FadeColorAfterDelay(System.Single,System.Int32)
extern void ManoVisualization_FadeColorAfterDelay_m813566DFED2493F4AEA1B089ED98F3C195D7364D ();
// 0x000000AE System.Void ManoVisualization::ToggleObjectVisibility(UnityEngine.GameObject)
extern void ManoVisualization_ToggleObjectVisibility_m0BB9713A850A08439B72E92AA270417A72F8ADA6 ();
// 0x000000AF System.Void ManoVisualization::InstantiateManomotionMeshes()
extern void ManoVisualization_InstantiateManomotionMeshes_m1776A1AAAADBA3FFB46327FEBF0D64AB3EF2726E ();
// 0x000000B0 System.Void ManoVisualization::ToggleTwoHandSupport()
extern void ManoVisualization_ToggleTwoHandSupport_m27FA87F8D56738B691892521AB1CC70662BB0A12 ();
// 0x000000B1 System.Void ManoVisualization::.ctor()
extern void ManoVisualization__ctor_m969DC29B4158F94BC3A7E58EDBCCCEDD30D97CE9 ();
// 0x000000B2 System.String ManomotionManager::get_Serial_key()
extern void ManomotionManager_get_Serial_key_mAC91B63D3EE4B0C3B1107FC36C78E93EF1D56DB5 ();
// 0x000000B3 System.Void ManomotionManager::set_Serial_key(System.String)
extern void ManomotionManager_set_Serial_key_mB3337B758AB3641FF662812A4C07A17224EA02CD ();
// 0x000000B4 System.Void ManomotionManager::init(System.String)
extern void ManomotionManager_init_m82B33D136988FE1A8894754FB658F4D8B7E79DF7 ();
// 0x000000B5 System.Void ManomotionManager::processFrame(HandInfo&,HandInfo&,Session&)
extern void ManomotionManager_processFrame_mD2FF6FA8BEEAD742CF05DE0FB03F5534AC3DAACD ();
// 0x000000B6 System.Void ManomotionManager::calibrate()
extern void ManomotionManager_calibrate_m43DA61AD6480E97EA36BFEA96231C7BDD653438B ();
// 0x000000B7 System.Void ManomotionManager::setFrameArray(UnityEngine.Color32[])
extern void ManomotionManager_setFrameArray_m3F1EB20C26C5F98D5EB27A60723096967F617DB4 ();
// 0x000000B8 System.Void ManomotionManager::setMRFrameArray(UnityEngine.Color32[],UnityEngine.Color32[])
extern void ManomotionManager_setMRFrameArray_mF83D2834630353DA8473449D9C546A570AA471D4 ();
// 0x000000B9 System.Void ManomotionManager::setResolution(System.Int32,System.Int32)
extern void ManomotionManager_setResolution_mFC7B3679A8D26B2F5D7E78C67C5297FEACEA1972 ();
// 0x000000BA System.Void ManomotionManager::setImageBinary(UnityEngine.Color32[])
extern void ManomotionManager_setImageBinary_m0140D81D43286771E47B3E075D1470BC87BD82BC ();
// 0x000000BB System.Int32 ManomotionManager::get_Processing_time()
extern void ManomotionManager_get_Processing_time_mF16313A60B4D6683577E0BE539848F6D1171738E ();
// 0x000000BC System.Int32 ManomotionManager::get_Fps()
extern void ManomotionManager_get_Fps_m173B41D7167AE97992B19CC62788BF5B2AE02ABB ();
// 0x000000BD System.Int32 ManomotionManager::get_Height()
extern void ManomotionManager_get_Height_m27B377FEE68F1E64094963856B88577323FAE75D ();
// 0x000000BE System.Int32 ManomotionManager::get_Width()
extern void ManomotionManager_get_Width_m1F664657C3C0F81DDBE3D1D3FE59A000C82A50B5 ();
// 0x000000BF System.Int32 ManomotionManager::get_Frame_number()
extern void ManomotionManager_get_Frame_number_m931EDF938311FB95D5A4778DEC8C5CD19043FD94 ();
// 0x000000C0 VisualizationInfo ManomotionManager::get_Visualization_info()
extern void ManomotionManager_get_Visualization_info_m72DA1F8668736DEE8C728F616F7C341899356BFB ();
// 0x000000C1 HandInfoUnity[] ManomotionManager::get_Hand_infos()
extern void ManomotionManager_get_Hand_infos_m274F74E85F89F2B7B7D919C310796653CF88E741 ();
// 0x000000C2 Session ManomotionManager::get_Manomotion_Session()
extern void ManomotionManager_get_Manomotion_Session_m13AB75678A9298A632A39A142B6D1C85965CED86 ();
// 0x000000C3 System.Void ManomotionManager::set_Manomotion_Session(Session)
extern void ManomotionManager_set_Manomotion_Session_m6C42E5075DF679E71F74F9662C7EDCA840A19486 ();
// 0x000000C4 ManomotionManager ManomotionManager::get_Instance()
extern void ManomotionManager_get_Instance_m16B8789855D612EA23037A8C69012E212770F0C4 ();
// 0x000000C5 System.Void ManomotionManager::SetSelectedHand(SelectedHand)
extern void ManomotionManager_SetSelectedHand_m0A1ED553683B3736A734CDCCE337B96D9C8EE4D9 ();
// 0x000000C6 System.Void ManomotionManager::Awake()
extern void ManomotionManager_Awake_m39843CEF82BA098F1F0DC06F115B3C45B892AB6F ();
// 0x000000C7 System.Void ManomotionManager::Start()
extern void ManomotionManager_Start_mC688DE0C5A2156A2F769A0E2123A1A4832830D9C ();
// 0x000000C8 System.Void ManomotionManager::SetCalibration(System.Int32)
extern void ManomotionManager_SetCalibration_mEE3856612506F2C703D2D0A1501B9E6EA88019BF ();
// 0x000000C9 System.Void ManomotionManager::StartWebCamTexture()
extern void ManomotionManager_StartWebCamTexture_mB7A06FD889157954AD1B737829394660F980ABD9 ();
// 0x000000CA System.Void ManomotionManager::PickResolution(System.Int32,System.Int32)
extern void ManomotionManager_PickResolution_mD331224269080FEEF7D4BD2F21023F444611869C ();
// 0x000000CB System.Void ManomotionManager::InstantiateSession()
extern void ManomotionManager_InstantiateSession_m049A653271CC616614B1D0DCA02761215D97157C ();
// 0x000000CC System.Void ManomotionManager::InstantiateHandInfos()
extern void ManomotionManager_InstantiateHandInfos_mA7D16C9F974CABD91A9151C625813942AB7A98FA ();
// 0x000000CD System.Void ManomotionManager::InstantiateVisualisationInfo()
extern void ManomotionManager_InstantiateVisualisationInfo_mEB689049944FEBC475134B4DA5E7B9F62CACD9C6 ();
// 0x000000CE System.Void ManomotionManager::InitiateTextures()
extern void ManomotionManager_InitiateTextures_mEB15982F8AB492229A9013FB6F128DD725A60776 ();
// 0x000000CF System.Void ManomotionManager::InitiateLibrary()
extern void ManomotionManager_InitiateLibrary_mD459C001D7269D2E75364B3951E02356C6C7D7BB ();
// 0x000000D0 System.Void ManomotionManager::SetVariables()
extern void ManomotionManager_SetVariables_mCBB745412CD7F29BB2692031EDB3F8F904D82EF0 ();
// 0x000000D1 System.Void ManomotionManager::SetHandInfo()
extern void ManomotionManager_SetHandInfo_m7F9E0FAFD77B15F6F04065DAB6D7CEEFBFD53650 ();
// 0x000000D2 System.Void ManomotionManager::SetVisualizationInfo()
extern void ManomotionManager_SetVisualizationInfo_m043332763932051CFAF4EA8BFD7D645E2E64E7E0 ();
// 0x000000D3 System.Void ManomotionManager::SetManoManagerVariables()
extern void ManomotionManager_SetManoManagerVariables_mF20A9EE81D2A602BA9BD0C5603CB35EA904DFB10 ();
// 0x000000D4 System.Void ManomotionManager::SetUnityConditions()
extern void ManomotionManager_SetUnityConditions_m65AC86829C300B45B8C1267C7844D6C1E093652F ();
// 0x000000D5 System.Void ManomotionManager::Init(System.String)
extern void ManomotionManager_Init_m6208887BF1B1CA9301706F08FBC6547DD8755452 ();
// 0x000000D6 System.Void ManomotionManager::SetImageBinaries()
extern void ManomotionManager_SetImageBinaries_mF130A6F2F777C1054DD2FDFF2F991BCD6683A3B5 ();
// 0x000000D7 System.Void ManomotionManager::SetResolution(System.Int32,System.Int32)
extern void ManomotionManager_SetResolution_mCA34F57D9544B6B18326F87A43676A64820978A0 ();
// 0x000000D8 System.Void ManomotionManager::SetFrameArray(UnityEngine.Color32[])
extern void ManomotionManager_SetFrameArray_mD9C00BE9C35B0083786D29FE4889BB3E8A2946BC ();
// 0x000000D9 System.Void ManomotionManager::SetMRFrameArray()
extern void ManomotionManager_SetMRFrameArray_m06573C2418204E69AA334FE36D6F1DC1FC989E61 ();
// 0x000000DA System.Void ManomotionManager::Update()
extern void ManomotionManager_Update_m906D4CE09BAA84D972262DA8B56249280101D0A0 ();
// 0x000000DB System.Void ManomotionManager::UpdateOrientation()
extern void ManomotionManager_UpdateOrientation_m199B0D2B3837E3B709833A1CDEC512AB471D0743 ();
// 0x000000DC System.Void ManomotionManager::UpdateTexturesWithNewInfo()
extern void ManomotionManager_UpdateTexturesWithNewInfo_m7519E593EDE352A13B71FC462C2B04817DA4BCFF ();
// 0x000000DD System.Void ManomotionManager::Set2HandSupport(System.Int32)
extern void ManomotionManager_Set2HandSupport_mA15763327707A7EFCFDFD5EB39F44C5F0FFBF2FA ();
// 0x000000DE System.Void ManomotionManager::SetBackgroundMode(BackgroundMode)
extern void ManomotionManager_SetBackgroundMode_m812EE2507A39EB4181653960E82A14ECBAFB5776 ();
// 0x000000DF System.Void ManomotionManager::UpdatePixelValues()
extern void ManomotionManager_UpdatePixelValues_m33F95BB8F25DDD3926B6526CFD2EF77FCD393142 ();
// 0x000000E0 System.Void ManomotionManager::ProcessManomotion()
extern void ManomotionManager_ProcessManomotion_mE61D40D512BABB97DB00B10CCDA8FAE3B5A01CA4 ();
// 0x000000E1 System.Void ManomotionManager::CalculateFPSAndProcessingTime()
extern void ManomotionManager_CalculateFPSAndProcessingTime_m78554B35E2305B1B1B3DF44CD32CB80442CBC6B7 ();
// 0x000000E2 System.Void ManomotionManager::CalculateProcessingTime()
extern void ManomotionManager_CalculateProcessingTime_m50DDD46A2267E2285A8062F3B48449F5A98798C3 ();
// 0x000000E3 System.Void ManomotionManager::ProcessFrame()
extern void ManomotionManager_ProcessFrame_m89BC645D41A7064201B06D8A1D519AFF87DB2F58 ();
// 0x000000E4 System.Void ManomotionManager::Calibrate()
extern void ManomotionManager_Calibrate_mEDA78D87AAC67AC1F9EB58F12FA8F066D30B6BA1 ();
// 0x000000E5 System.Void ManomotionManager::.ctor()
extern void ManomotionManager__ctor_m26AD6DA6717A76B9B91D9F336342A3C1BA7114BF ();
// 0x000000E6 System.Void ManomotionUIManagment::Start()
extern void ManomotionUIManagment_Start_mB16EEC83783B55FBC8D820F9E39028B8DBA15A59 ();
// 0x000000E7 System.Void ManomotionUIManagment::Update()
extern void ManomotionUIManagment_Update_m6B9B0FFFD00B5F5EFB4DAFC267E5331193D93D7B ();
// 0x000000E8 System.Void ManomotionUIManagment::ToggleUIElement(UnityEngine.GameObject)
extern void ManomotionUIManagment_ToggleUIElement_m6300FB9BF4EFEC09A03E1FE2EA683EDB640BFFFB ();
// 0x000000E9 System.Void ManomotionUIManagment::UpdateFPSText()
extern void ManomotionUIManagment_UpdateFPSText_mB75484D4557080B335D882808A1ADA21D934F274 ();
// 0x000000EA System.Void ManomotionUIManagment::UpdateProcessingTime()
extern void ManomotionUIManagment_UpdateProcessingTime_mF5E2A1765A729BAEC5B05E2E2B935B77AD3DDBB5 ();
// 0x000000EB System.Void ManomotionUIManagment::HideHandButtons()
extern void ManomotionUIManagment_HideHandButtons_m7CFEABE49349A506C2D927863C82E20BBB4D16DD ();
// 0x000000EC System.Void ManomotionUIManagment::ShowHandButtons()
extern void ManomotionUIManagment_ShowHandButtons_m5F92BC43CBB717E645DA783DA5C394290A695EC1 ();
// 0x000000ED System.Void ManomotionUIManagment::.ctor()
extern void ManomotionUIManagment__ctor_m79A86923B7BFA05BAF4E2E395086DF1A4DB87ED2 ();
// 0x000000EE System.Boolean MenuManager::get_MenuIsOpen()
extern void MenuManager_get_MenuIsOpen_m2E657B2CC8985DCA098182132F1432A7D8AB45E5 ();
// 0x000000EF System.Void MenuManager::set_MenuIsOpen(System.Boolean)
extern void MenuManager_set_MenuIsOpen_m3D230C8ABC29B86AFE0EF1B5A7A92FEC8CE89AEB ();
// 0x000000F0 System.Void MenuManager::Start()
extern void MenuManager_Start_m253720A589CA2ECFD9BEECCEF10D8D0D0E1967F3 ();
// 0x000000F1 System.Void MenuManager::ToggleIconsMenu()
extern void MenuManager_ToggleIconsMenu_m87517B43730D1D08A49534DC41BF3686844C7BD4 ();
// 0x000000F2 System.Void MenuManager::CloseMenu()
extern void MenuManager_CloseMenu_mEAEBF1C7DA3AC8DE76CB08EA49363879C2DC1019 ();
// 0x000000F3 System.Void MenuManager::.ctor()
extern void MenuManager__ctor_mF9508A23E828387B76DB818A2E34D3A8AC9063EF ();
// 0x000000F4 System.Void ToggleGizmos::Start()
extern void ToggleGizmos_Start_m6EF259CDB9BC897C98D4ED60BF5B33127AB5D524 ();
// 0x000000F5 System.Void ToggleGizmos::ToggleShowRotation()
extern void ToggleGizmos_ToggleShowRotation_mAEAC05E16E8E31E2CE209D73675452406ABB9007 ();
// 0x000000F6 System.Void ToggleGizmos::ToggleShowDepth()
extern void ToggleGizmos_ToggleShowDepth_mC2BBFBDF463D940078B332EBCFFC033D8F19D9D1 ();
// 0x000000F7 System.Void ToggleGizmos::ToggleShowHandStates()
extern void ToggleGizmos_ToggleShowHandStates_mB19799A41198FBDCA7D5562B24B0E5DEAC49FE76 ();
// 0x000000F8 System.Void ToggleGizmos::ToggleShowManoclass()
extern void ToggleGizmos_ToggleShowManoclass_m069ED58FF81A5093ADD47DED0C76451B3A729A4B ();
// 0x000000F9 System.Void ToggleGizmos::ToggleShowTriggerGesture()
extern void ToggleGizmos_ToggleShowTriggerGesture_m50321C789A2F7F27A4AA716CB07855564D32283B ();
// 0x000000FA System.Void ToggleGizmos::ToggleShowContinuousGesture()
extern void ToggleGizmos_ToggleShowContinuousGesture_mC085BAA9B0C2FED0E9DCEA919C6FC25D01E3A4FB ();
// 0x000000FB System.Void ToggleGizmos::ToggleBoundingBox()
extern void ToggleGizmos_ToggleBoundingBox_m42EB3F2F6811ED44C82098C567FD0B713B1F2E23 ();
// 0x000000FC System.Void ToggleGizmos::ToggleShowFlags()
extern void ToggleGizmos_ToggleShowFlags_mA26E955E16F9C5D8756AC99D2A9F7A3B9A31AD56 ();
// 0x000000FD System.Void ToggleGizmos::.ctor()
extern void ToggleGizmos__ctor_m29BD44E68C1448C53603AEF5D2CFA2153481C7B9 ();
// 0x000000FE System.Void ToggleVisualizationValues::Start()
extern void ToggleVisualizationValues_Start_mCE8F24893A402C2B767C2D1BFE38FD538E1F6F0B ();
// 0x000000FF System.Void ToggleVisualizationValues::ToggleShowInner()
extern void ToggleVisualizationValues_ToggleShowInner_m66910BF2ACF4D40EA9DB13AA2D64843E1478A5A0 ();
// 0x00000100 System.Void ToggleVisualizationValues::ToggleShowContour()
extern void ToggleVisualizationValues_ToggleShowContour_m6CC17D51B273DB1AD9292F81243B09C49C3B83D1 ();
// 0x00000101 System.Void ToggleVisualizationValues::ToggleShowPalmCenter()
extern void ToggleVisualizationValues_ToggleShowPalmCenter_mC8DF4EA4280BEBECC542DAB5600E95ED6AAEFDAD ();
// 0x00000102 System.Void ToggleVisualizationValues::ToggleShowFingertips()
extern void ToggleVisualizationValues_ToggleShowFingertips_m7527EA86AB22964D534D357322F7303290A61218 ();
// 0x00000103 System.Void ToggleVisualizationValues::ToggleShowFingertipLabels()
extern void ToggleVisualizationValues_ToggleShowFingertipLabels_m1043E3A83AB8E15B325384D5A08CE9AC9FBBE43E ();
// 0x00000104 System.Void ToggleVisualizationValues::ToggleShowHandLayer()
extern void ToggleVisualizationValues_ToggleShowHandLayer_m79BB727F252131D1604FB73593A7AF16E8CDA439 ();
// 0x00000105 System.Void ToggleVisualizationValues::ToggleShowBackgroundLayer()
extern void ToggleVisualizationValues_ToggleShowBackgroundLayer_mE90739FC3952B0075ADFFA5A509C5DDD47336473 ();
// 0x00000106 System.Void ToggleVisualizationValues::ToggleBoundingBox()
extern void ToggleVisualizationValues_ToggleBoundingBox_mD2E580ABB1BF6F2D9A9799CCAA6BA8899C51D9A0 ();
// 0x00000107 System.Void ToggleVisualizationValues::.ctor()
extern void ToggleVisualizationValues__ctor_mDF980F01D04103EE5E9F76E7511CCD07004CCC12 ();
// 0x00000108 System.Void TwoHandSupport::Update()
extern void TwoHandSupport_Update_m806FE846407D2E0B3585929BFFD7BB3E83DFB3D0 ();
// 0x00000109 System.Void TwoHandSupport::.ctor()
extern void TwoHandSupport__ctor_m0A341DE9C2CC94A3442CE7B499798378AF8F1DE4 ();
// 0x0000010A System.Void UIIconBehavior::Start()
extern void UIIconBehavior_Start_m92DFD47A95DD088893833B2C2A5D97ACC3DF39C8 ();
// 0x0000010B System.Void UIIconBehavior::UpdateIconAndFrame(System.Boolean)
extern void UIIconBehavior_UpdateIconAndFrame_m8D62BF99124B26B57184F4821A6CA9639DB373DE ();
// 0x0000010C System.Void UIIconBehavior::ToggleActive()
extern void UIIconBehavior_ToggleActive_m71245B86C09385A6960B4F56AC3CFB7DD712A391 ();
// 0x0000010D System.Void UIIconBehavior::DeactivateIcon()
extern void UIIconBehavior_DeactivateIcon_m65FD6202060FDDB1B02DB05BE2D3195DBCEE4A77 ();
// 0x0000010E System.Void UIIconBehavior::.ctor()
extern void UIIconBehavior__ctor_m008D18128F400BA12BDD5937A2F11299186DF1B9 ();
// 0x0000010F System.Void UIRotationBehaviour::add_OnMenuEnabled(UIRotationBehaviour_MenuEnabled)
extern void UIRotationBehaviour_add_OnMenuEnabled_m24E9F3217FD401B121E7C4FD4076AF0AEABE2B17 ();
// 0x00000110 System.Void UIRotationBehaviour::remove_OnMenuEnabled(UIRotationBehaviour_MenuEnabled)
extern void UIRotationBehaviour_remove_OnMenuEnabled_m98FB9E8D89A9AD892EC7D00ED4B055ACEE8DC9BD ();
// 0x00000111 System.Void UIRotationBehaviour::OnEnable()
extern void UIRotationBehaviour_OnEnable_mADBC5C9E498B1876297C176D5C8AC9FAB8EA604D ();
// 0x00000112 System.Void UIRotationBehaviour::.ctor()
extern void UIRotationBehaviour__ctor_m077BCE313BC9787B9C9DAE8150022B9D98CAB1E1 ();
// 0x00000113 System.Void appearOnEnable::OnEnable()
extern void appearOnEnable_OnEnable_mA138307841C893900D280B4F5538352107F8649B ();
// 0x00000114 System.Void appearOnEnable::.ctor()
extern void appearOnEnable__ctor_mF7337B725D054D41CEFE52D12DAFF0AFC248B650 ();
// 0x00000115 System.Void BWEffect::Awake()
extern void BWEffect_Awake_mBC996F2191595DF4A18AE93014CC90D946E93EF6 ();
// 0x00000116 System.Void BWEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BWEffect_OnRenderImage_mDD7CA2A3613B17F46B599FEDFF37656882671249 ();
// 0x00000117 System.Void BWEffect::.ctor()
extern void BWEffect__ctor_mBB8B592FB1FE69C7E1893BFCE2E941ABE8E016BB ();
// 0x00000118 System.Void GrabPass::Awake()
extern void GrabPass_Awake_m3E8E65B6374F3CFCF2773781F4DA01D550D9D6CC ();
// 0x00000119 System.Void GrabPass::Update()
extern void GrabPass_Update_mD1CC7CB04FC67A2755204B0956FF98420169D625 ();
// 0x0000011A System.Void GrabPass::.ctor()
extern void GrabPass__ctor_m9D45212AB7B3CC10ADA32F622751A2C3FB3FFBF9 ();
// 0x0000011B System.Void Grayscale::Awake()
extern void Grayscale_Awake_m7F697CE268A4589F84FA37033A5D011F999A94BD ();
// 0x0000011C System.Void Grayscale::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Grayscale_OnRenderImage_m74CACBDDC52F0C488DCEF06E5216C66CEF89BC16 ();
// 0x0000011D System.Void Grayscale::.ctor()
extern void Grayscale__ctor_m68C4145AEFE0835F1A7EAC46B1D8639EF077B5CF ();
// 0x0000011E System.Void Category_<Calculate>d__19::.ctor(System.Int32)
extern void U3CCalculateU3Ed__19__ctor_m615A120FE639686915DE54EE3BA56E3FBD5AE822 ();
// 0x0000011F System.Void Category_<Calculate>d__19::System.IDisposable.Dispose()
extern void U3CCalculateU3Ed__19_System_IDisposable_Dispose_m1D420E592E255D0E785A8EF71B75A3855576C129 ();
// 0x00000120 System.Boolean Category_<Calculate>d__19::MoveNext()
extern void U3CCalculateU3Ed__19_MoveNext_mC2B6BE17903B2D10CE286E863C5E46C7E4AB4D67 ();
// 0x00000121 System.Object Category_<Calculate>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCalculateU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m364515E3E7A8648D08860567D8798AFC1E093829 ();
// 0x00000122 System.Void Category_<Calculate>d__19::System.Collections.IEnumerator.Reset()
extern void U3CCalculateU3Ed__19_System_Collections_IEnumerator_Reset_m6B702192F6C67CA73EC6DB10D2E0C88C5AA8068E ();
// 0x00000123 System.Object Category_<Calculate>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CCalculateU3Ed__19_System_Collections_IEnumerator_get_Current_m36978B32662276698B16DEE1EF99480FEE454CDD ();
// 0x00000124 System.Void Category_<AlignIcons>d__20::.ctor(System.Int32)
extern void U3CAlignIconsU3Ed__20__ctor_m82E26C81F63C7C8FA66A705447515BC322380D93 ();
// 0x00000125 System.Void Category_<AlignIcons>d__20::System.IDisposable.Dispose()
extern void U3CAlignIconsU3Ed__20_System_IDisposable_Dispose_mB035ED02E82EC493AB05589CD8C88A0E74CD734E ();
// 0x00000126 System.Boolean Category_<AlignIcons>d__20::MoveNext()
extern void U3CAlignIconsU3Ed__20_MoveNext_m3E284721779CFB785F294CC73AC1493D3D72ADD3 ();
// 0x00000127 System.Object Category_<AlignIcons>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAlignIconsU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDC8A47A814C55CAB1BB8A3D82452F0BF2D88EBA ();
// 0x00000128 System.Void Category_<AlignIcons>d__20::System.Collections.IEnumerator.Reset()
extern void U3CAlignIconsU3Ed__20_System_Collections_IEnumerator_Reset_m1815BB7ACAA4C9A6CEF1157C3D1577140A084DBC ();
// 0x00000129 System.Object Category_<AlignIcons>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CAlignIconsU3Ed__20_System_Collections_IEnumerator_get_Current_mAF6B74762DAB34C74D41465CDE77C3B06411350B ();
// 0x0000012A System.Void CategoryManager_<PositionCategoriesAfter>d__10::.ctor(System.Int32)
extern void U3CPositionCategoriesAfterU3Ed__10__ctor_mADFE7016D30E1B41DBFAC253035F4AC67D2EED52 ();
// 0x0000012B System.Void CategoryManager_<PositionCategoriesAfter>d__10::System.IDisposable.Dispose()
extern void U3CPositionCategoriesAfterU3Ed__10_System_IDisposable_Dispose_m8044732A5DAE6BAB651F5206913AF230E94E9611 ();
// 0x0000012C System.Boolean CategoryManager_<PositionCategoriesAfter>d__10::MoveNext()
extern void U3CPositionCategoriesAfterU3Ed__10_MoveNext_m489DE25363FA0B6A64269123AB81636455F4E5D0 ();
// 0x0000012D System.Object CategoryManager_<PositionCategoriesAfter>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPositionCategoriesAfterU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58029AD776E76779D9AD2255A0F4C71A906D326F ();
// 0x0000012E System.Void CategoryManager_<PositionCategoriesAfter>d__10::System.Collections.IEnumerator.Reset()
extern void U3CPositionCategoriesAfterU3Ed__10_System_Collections_IEnumerator_Reset_m4EA2A9A62FB45B05862DB6600D6D27A5192A616B ();
// 0x0000012F System.Object CategoryManager_<PositionCategoriesAfter>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CPositionCategoriesAfterU3Ed__10_System_Collections_IEnumerator_get_Current_mE691FC30566BE55EB8AC42B74F1E1F6D9F085C13 ();
// 0x00000130 System.Void ChooseBackgroundBehavior_<CloseAvailableBackgroundMenuAfter>d__11::.ctor(System.Int32)
extern void U3CCloseAvailableBackgroundMenuAfterU3Ed__11__ctor_m92C7B4A24C6A55BC32663B7E5390B77CA1B75C46 ();
// 0x00000131 System.Void ChooseBackgroundBehavior_<CloseAvailableBackgroundMenuAfter>d__11::System.IDisposable.Dispose()
extern void U3CCloseAvailableBackgroundMenuAfterU3Ed__11_System_IDisposable_Dispose_mC1F66B8A3AA04EE3C373F799590C00FE4BBF391D ();
// 0x00000132 System.Boolean ChooseBackgroundBehavior_<CloseAvailableBackgroundMenuAfter>d__11::MoveNext()
extern void U3CCloseAvailableBackgroundMenuAfterU3Ed__11_MoveNext_m938942CD9CBB21D249CD2F022C022D0EBADAD0B0 ();
// 0x00000133 System.Object ChooseBackgroundBehavior_<CloseAvailableBackgroundMenuAfter>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCloseAvailableBackgroundMenuAfterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46345104AB7BD9EA9A8B9979C16E973BFA99232F ();
// 0x00000134 System.Void ChooseBackgroundBehavior_<CloseAvailableBackgroundMenuAfter>d__11::System.Collections.IEnumerator.Reset()
extern void U3CCloseAvailableBackgroundMenuAfterU3Ed__11_System_Collections_IEnumerator_Reset_m6A38DB620251104181527F64375443A8EDAFAD2E ();
// 0x00000135 System.Object ChooseBackgroundBehavior_<CloseAvailableBackgroundMenuAfter>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CCloseAvailableBackgroundMenuAfterU3Ed__11_System_Collections_IEnumerator_get_Current_m889A2A5466E4CF12ED26F2A08EA1DCDF855C4D01 ();
// 0x00000136 System.Void ManoCalibration_<Calibrate>d__20::.ctor(System.Int32)
extern void U3CCalibrateU3Ed__20__ctor_m93DCB7494AF7160C6A0285AE4644BAA8AA48D184 ();
// 0x00000137 System.Void ManoCalibration_<Calibrate>d__20::System.IDisposable.Dispose()
extern void U3CCalibrateU3Ed__20_System_IDisposable_Dispose_m7E9602E82155B514D0BABF8572E33EAE4739B4CD ();
// 0x00000138 System.Boolean ManoCalibration_<Calibrate>d__20::MoveNext()
extern void U3CCalibrateU3Ed__20_MoveNext_m1A2C5252919F19BCE26D847FB532C4DAB6FCABAE ();
// 0x00000139 System.Object ManoCalibration_<Calibrate>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCalibrateU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05BEBAD7B3A9F6E4D36C979E17BD37955AD6BE58 ();
// 0x0000013A System.Void ManoCalibration_<Calibrate>d__20::System.Collections.IEnumerator.Reset()
extern void U3CCalibrateU3Ed__20_System_Collections_IEnumerator_Reset_m8655E78450BEB05E3A541FEB57751F6F0942D74B ();
// 0x0000013B System.Object ManoCalibration_<Calibrate>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CCalibrateU3Ed__20_System_Collections_IEnumerator_get_Current_mF6801B748FE673463CAF253E3B1E8DE825A53115 ();
// 0x0000013C System.Void ManoEvents_ManoEvent::.ctor(System.Object,System.IntPtr)
extern void ManoEvent__ctor_mC87803C22932E257AC870C00BB24E867C538024A ();
// 0x0000013D System.Void ManoEvents_ManoEvent::Invoke()
extern void ManoEvent_Invoke_m151FAD05D37CDF2B3ADEDDACD8DDBFD7D1C16352 ();
// 0x0000013E System.IAsyncResult ManoEvents_ManoEvent::BeginInvoke(System.AsyncCallback,System.Object)
extern void ManoEvent_BeginInvoke_m578ADA3488A4F864241560632FC8D017E75C07DB ();
// 0x0000013F System.Void ManoEvents_ManoEvent::EndInvoke(System.IAsyncResult)
extern void ManoEvent_EndInvoke_m4755FDB3C8BB55FBAB8F634F5D538F9596C6FDB9 ();
// 0x00000140 System.Void ManoEvents_ManoEvent`1::.ctor(System.Object,System.IntPtr)
// 0x00000141 System.Void ManoEvents_ManoEvent`1::Invoke(T)
// 0x00000142 System.IAsyncResult ManoEvents_ManoEvent`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x00000143 System.Void ManoEvents_ManoEvent`1::EndInvoke(System.IAsyncResult)
// 0x00000144 System.Void ManoEvents_ManoEvent`2::.ctor(System.Object,System.IntPtr)
// 0x00000145 System.Void ManoEvents_ManoEvent`2::Invoke(T1,T2)
// 0x00000146 System.IAsyncResult ManoEvents_ManoEvent`2::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
// 0x00000147 System.Void ManoEvents_ManoEvent`2::EndInvoke(System.IAsyncResult)
// 0x00000148 System.Void ManoUtils_OrientationChange::.ctor(System.Object,System.IntPtr)
extern void OrientationChange__ctor_m37AFD52A8B8C6E36F81B889C33012541FE4F98A3 ();
// 0x00000149 System.Void ManoUtils_OrientationChange::Invoke()
extern void OrientationChange_Invoke_m99EC564D0A365E26B20602830313CA359A2CC8E0 ();
// 0x0000014A System.IAsyncResult ManoUtils_OrientationChange::BeginInvoke(System.AsyncCallback,System.Object)
extern void OrientationChange_BeginInvoke_m8E446690B43886B7ED131292713922588B82A26E ();
// 0x0000014B System.Void ManoUtils_OrientationChange::EndInvoke(System.IAsyncResult)
extern void OrientationChange_EndInvoke_mCC4375836A62027607889771E3EF7268D15154A0 ();
// 0x0000014C System.Void ManoVisualization_<FadeColorAfterDelay>d__101::.ctor(System.Int32)
extern void U3CFadeColorAfterDelayU3Ed__101__ctor_m311B303671CC42F81CCD7C87DF5DF4D8F6F5B2C1 ();
// 0x0000014D System.Void ManoVisualization_<FadeColorAfterDelay>d__101::System.IDisposable.Dispose()
extern void U3CFadeColorAfterDelayU3Ed__101_System_IDisposable_Dispose_m1F89791A19770A456830A2B4C7000B3F720CECE8 ();
// 0x0000014E System.Boolean ManoVisualization_<FadeColorAfterDelay>d__101::MoveNext()
extern void U3CFadeColorAfterDelayU3Ed__101_MoveNext_m747EB635C916D60FBDC70D3DECBB6753AC2815FC ();
// 0x0000014F System.Object ManoVisualization_<FadeColorAfterDelay>d__101::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeColorAfterDelayU3Ed__101_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EB557C8672AF496D303ACF2DC46C9275B26D2A7 ();
// 0x00000150 System.Void ManoVisualization_<FadeColorAfterDelay>d__101::System.Collections.IEnumerator.Reset()
extern void U3CFadeColorAfterDelayU3Ed__101_System_Collections_IEnumerator_Reset_mA85495DB7CC52E97315591C1B97C60CCAC450809 ();
// 0x00000151 System.Object ManoVisualization_<FadeColorAfterDelay>d__101::System.Collections.IEnumerator.get_Current()
extern void U3CFadeColorAfterDelayU3Ed__101_System_Collections_IEnumerator_get_Current_mF1360C8B3539659C517D847824CA6A9ABC43B446 ();
// 0x00000152 System.Void UIRotationBehaviour_MenuEnabled::.ctor(System.Object,System.IntPtr)
extern void MenuEnabled__ctor_m90B53FF12A4F3A207C95F2EC8F3B477B12F0D34D ();
// 0x00000153 System.Void UIRotationBehaviour_MenuEnabled::Invoke()
extern void MenuEnabled_Invoke_m9D222AE00791440547FC3224AE97C5365B519589 ();
// 0x00000154 System.IAsyncResult UIRotationBehaviour_MenuEnabled::BeginInvoke(System.AsyncCallback,System.Object)
extern void MenuEnabled_BeginInvoke_mD22C9B0D4B64F729459423498186D8AC62927F5B ();
// 0x00000155 System.Void UIRotationBehaviour_MenuEnabled::EndInvoke(System.IAsyncResult)
extern void MenuEnabled_EndInvoke_mB44125F61BD08551A8DD67B332EB6AD90FFE4ACB ();
static Il2CppMethodPointer s_methodPointers[341] = 
{
	ApplicationManager_get_Instance_mB8F659A6B45A109C0071C1FAE76933A21E0442C9,
	ApplicationManager_set_Instance_m456375F0247E0D44E2AE18F1CC5C9665E4B7DC28,
	ApplicationManager_Awake_m9EF712D0014298A38ED83E2400DE69C3D623D7A3,
	ApplicationManager_Start_m7D8D7EA6EFF8164E3DCEDB45690D575D56314527,
	ApplicationManager_DisplayInstructions_m7013ECAE6A72367DD985DBF073554CF3777BE895,
	ApplicationManager_StartMainApplication_m8028C0403F4181E9A9092C256C81B2B214DDBB93,
	ApplicationManager__ctor_mC8025C842679B2F85EC46E91B63EB9E2B360F1C9,
	InstructionsManager_get_Instance_m9D8D043EDC3912EC2F98D81BFCB63BE21E3FA123,
	InstructionsManager_set_Instance_mB4D51BE565A9BD878F12708CF5DC8E8C312D9164,
	InstructionsManager_Awake_m082C1F24FAD438E93F01E0FD04E70E7A76CC842D,
	InstructionsManager_IncreaseSlide_mAAA5865E17F2FFCA24B13A870889427BFFBD816B,
	InstructionsManager_DecreaseSlide_m7A4F9FE41B4027E52302A9F64BD3A7717E4022EA,
	InstructionsManager_ShowSlide_mD63046B258A6409CF1AEAD59BFED86B2B834A784,
	InstructionsManager_StartInstructions_m62FF2A3A86B131F420F46901A351F6C9EC0795EA,
	InstructionsManager_ForceInstructions_m1488E2B75607F1AC69C073FB233D05BAAC511724,
	InstructionsManager_UpdateVisualComponents_m4154B16FF9DB13F92B6792B35746E12DB1404304,
	InstructionsManager_HighlightCircles_mD854126CE1AC9AF42933BD0899BD4966DD1DCC3B,
	InstructionsManager_SetCircleVisibility_m6ED7D7F88686169AC4BAC953092B07E42E67FA8B,
	InstructionsManager_ShowManomotionComponents_mEBB1CAA15C84065014091234A9B16C69B1366A1A,
	InstructionsManager_HideManomotionComponents_m2EB01DB7184F8A538C55A05E69B67D4D22142F79,
	InstructionsManager_StopShowingInstructions_m52F4453D6627C61A26F992160CA7DC76922A6BC5,
	InstructionsManager_DisplayVisualComponents_mDD1B8F44725E5167B3FB50D9205B4F4CDA7EBACF,
	InstructionsManager_GetPreviousManoVisualizationValues_mB0AD79F161D791E196755C88FEA51C82C2D233AF,
	InstructionsManager_TurnOffManovizualizationValues_m07177FF1664DCB846B3692C0B96247857AFADFC5,
	InstructionsManager_ReInstateVizualizationValues_m12F0AF964E6761AB0F8E5BA674B1BEC5FA1E070C,
	InstructionsManager__ctor_m3DA29F5F41B072CD557D22AA3620855B811957BF,
	SwipeInput_Update_m7FDBD7B46AAD627A4CCBC8E3AB5E4F052207C5A3,
	SwipeInput_DetectSwipe_m56C6D853C706B5E25734F773713DBA4B69060CBF,
	SwipeInput_EvaluateTouch_m3F2FA5ACBC08D242BAAD4C1D94A55812EE7B7B3F,
	SwipeInput__ctor_m5A1F2147276C5FA9AD5A0F7D3581F5BE24CA05E4,
	BackgroundModeButtonSelector_Start_m5523414EDBAA77CC11F67C4684C0D49DC44D5401,
	BackgroundModeButtonSelector_SelectBackgroundMode_mD89DD15F1DFCEEE08D412421D8FEE52237704FFA,
	BackgroundModeButtonSelector__ctor_m37EDBBF4CF015A46EEBACAB4A600F41594385521,
	BoundingBoxUI_Start_m20B0EF9A6C9B720F67AF71AE57E2ECB7C68E4858,
	BoundingBoxUI_UpdateInfo_m58382D879F88EB5E7FDEF512ABC96FDF20E4C64F,
	BoundingBoxUI__ctor_mD9728406F85083558C3A83AD3604C6159B8720E8,
	CalibrationArea_OnMouseOver_mEE8609F0B076370A9CAAFC359B938432614DB51A,
	CalibrationArea_OnMouseExit_m2FCDD03037F28D7495E296DED3B2D7A468D2CF41,
	CalibrationArea__ctor_m7BA4249E5F23504A41012F48288CD35C2A3F0712,
	Category_Start_m0F38089C16A6ACA99234D26FDBCACFD6A2EB0199,
	Category_InitializeUIValues_mC6C41DCD2E12844BA7E763556E71AA96D6CB1594,
	Category_CalculateDimensions_mFCDAFB9451AB54A10C8532F8C62B31585B2C76A8,
	Category_Calculate_m6D351EA154FA9FB0B68B104014F1418150C8C7C0,
	Category_AlignIcons_m5510ACA8D7ADF3CAD15E995F2669A5E253A1B7E0,
	Category__ctor_mD0975E34D2A70F6F461D35938B64928A717B632B,
	CategoryManager_get_Instance_mCCC1362A3203C63D8860CFBAAD47A3ECEF5541D9,
	CategoryManager_set_Instance_m9EF23C5927357F02A9BF24AAA502EB5711674DE7,
	CategoryManager_Awake_m7CB34D7DC418E1A6CAAD8274F68A47DF441B9780,
	CategoryManager_Start_m303747026E43DE04C5D84351B431E807692FA0F7,
	CategoryManager_PositionCategories_m7D5CE8A962325C1CAA24AB0DAB3FB326CD0B50CC,
	CategoryManager_PositionCategoriesAfter_mFD96F40B6E8C2216FA3C0F169E9455399E3DE2B0,
	CategoryManager__ctor_m403FDA37217ADBF38AEDC15C70AE9B9F09232A30,
	ChooseBackgroundBehavior_get_Instance_m28E9803E4A6C1766FD6EB3D2D7E30E3DB38F34CA,
	ChooseBackgroundBehavior_Start_m40D84EA16B265D95EA598BB59AD8EA7E02A6D3DA,
	ChooseBackgroundBehavior_CloseAvailableBackgroundMenu_m8394410C6D62A9CC9F75D7C6173DBB3AEB477956,
	ChooseBackgroundBehavior_ToggleAvailableBackgroundsMenu_m16F7523808F215291D6D896E7077FD1278FEC29F,
	ChooseBackgroundBehavior_CloseAvailableBackgroundMenuAfter_mACE318610FED9914A59B93380223224CC9483860,
	ChooseBackgroundBehavior__ctor_m157F0277C120464D82ED4312D39EA1A5EC33FADE,
	GizmoManager_get_Instance_m3F54A2BAEA6C642DFA10798032199510CD75329C,
	GizmoManager_set_Instance_mC96FCB92A4B88053FF84E7594A50219B9F40AC46,
	GizmoManager_get_Show_flags_m641657D32F5B9AB82EA731F42824057B4C3D955B,
	GizmoManager_set_Show_flags_m89F5C774632AFFFADC6EC2976097F7D195D35048,
	GizmoManager_get_Show_hand_states_m0978DA33364266CBEE7081FA3A946CA0BD1B2DD1,
	GizmoManager_set_Show_hand_states_mA34EC66F3E7E7C42960EFB2858A2CCA5AB643E5B,
	GizmoManager_get_Show_rotation_mF8E147BA88A2995F5096E27039E0FC0CD2025D31,
	GizmoManager_set_Show_rotation_m104D6C640B6964A187B7A0343C4487386194777A,
	GizmoManager_get_Show_mano_class_m8CC99CAB42DAB9D058CE823E5D7D6EC3EE8E69CB,
	GizmoManager_set_Show_mano_class_m8B1F38E2571B84F8341C7FC0853FBB4168F4C1C6,
	GizmoManager_get_Show_trigger_gesture_mD0E38DC389CBCA09078FC499FCF4C82ACBE1EF96,
	GizmoManager_set_Show_trigger_gesture_mE90EA80C5810BAA05F48414756A5489AA5243062,
	GizmoManager_get_Show_continuous_gesture_mE9AE6F6D7DD2C2ABAFAAE24E3BF335058FB63334,
	GizmoManager_set_Show_continuous_gesture_m26078B571466290E4677573A9B0CA3FDC85B6E91,
	GizmoManager_get_Show_bounding_box_m11605685960A7F81F83A7277FCA866D8E182DED8,
	GizmoManager_set_Show_bounding_box_m5DE3BBBC5084152FFC6267129C05607691205A63,
	GizmoManager_get_Show_depth_m225374A4805DAC0476379D8CBF272370F0A39F4C,
	GizmoManager_set_Show_depth_m8228D6C89FA67B992A8E8C81998ED49D4CE8F63F,
	GizmoManager_Start_m980F0EB0DC1C46B0D6959BD0B0052546AFFCB8C4,
	GizmoManager_Update_m0C72B393569346BBD2E1B751DCA04C2416B75C37,
	GizmoManager_DisplayFlags_m208D15B0F22CBFB1FEE4DC41D0C645CBE570F0AE,
	GizmoManager_DisplayRotationGizmo_m55EA5B432E0D76C8D018A51047E68A972CE495B2,
	GizmoManager_DisplayManoclass_m180417D8CFCBEEDF6242882B4054BBB1A49DFD5D,
	GizmoManager_DisplayTriggerGesture_mEB2422AA9F61480FC4F71F164ADF556D1EA05862,
	GizmoManager_DisplayContinuousGesture_m626CFA07DCE60DA7CA3545CC77847483E01CF56A,
	GizmoManager_DisplayHandState_mB974E7A84B9956FBD38A0925B11AAAA27DDFDB84,
	GizmoManager_DisplayDepth_mFAFF6476877011441D74C23962F884EDCE325CB9,
	GizmoManager_HighlightStatesToStateDetection_m06CCE4F7A99F4B4E978B3BD3B6C3535EE4360AA3,
	GizmoManager_SetRotationGizmoParts_mDD3E98D9E0D79CFE44A67ED5221F37AC2A984803,
	GizmoManager_SetDepthGizmoParts_mFE3697AA8645B8AB77BD7FAD2DF81B810DCA8A56,
	GizmoManager_SetGestureDescriptionParts_mD16BED649CEE58FCEE8376311B4D874D3BBDD38F,
	GizmoManager__ctor_m04AC7489BBFD5D5E2FBA059B42909D8772620825,
	HandDetectionModeButtonBehavior_Start_m1E576EEB383203280B1C5FC25B7059CC945CBB12,
	HandDetectionModeButtonBehavior_SelectHandDetectionMode_mC03005C5650E26D54090BFA6F5D03315BC2CB924,
	HandDetectionModeButtonBehavior__ctor_m7C8CCEC5C3846E46ABE6BC559A0D178170EF844A,
	InteractionArea_Start_m04DAFA9F123C6391996F25E92B29EAF527E47C76,
	InteractionArea_EnterMouse_m664634F32B30EC8631EB19B7E6FA7F11C6B6905C,
	InteractionArea_ExitMouse_m29F93B4F9C3000D8A70657740FF7DDEF1B0BCDC3,
	InteractionArea__ctor_mB0DA799B39871257C14FE5E5AB650BCAD454A4D4,
	ManoCalibration_get_Instance_mA30B72661E2A0336B046455E3519AF4B36D1AD86,
	ManoCalibration_set_Instance_mA6374FA32BA884D5C92C14033A915970AA408834,
	ManoCalibration_Start_mA8E306E4C4D8600ECB96CB933AF7CC8AF6FBA386,
	ManoCalibration_Update_m1E279E3B5E106A703CF4935BF4E8196E192ECEC8,
	ManoCalibration_SaveCalibration_m98F8397F9BCD8C780C78C609AA6BCE411E69E573,
	ManoCalibration_LoadCalibration_mA9B8E3701C8B1AA796D3C7692F2DB51EE41AE840,
	ManoCalibration_DetectCalibration_m5BE9C75D3F1B8DAF0C385EE7FB62AD0BD6F1C36B,
	ManoCalibration_Calibrate_m987B0F97D53CF7A2A27DB21C42FA96EC4C053A8A,
	ManoCalibration__ctor_mE430F4F7FDB3093D8B3BFD0DEBBF84564E87782F,
	ManoEvents_get_Instance_mA0C3DDC467A0A11BEEF0BEE56C1C1F237B73330D,
	ManoEvents_set_Instance_m5164042F083381A1016230F27677092D4491ED7D,
	ManoEvents_add_OnCalibrationSuccess_m9D870E98C348B52863905C3E52A0D692785B270D,
	ManoEvents_remove_OnCalibrationSuccess_m4171FCDBAAFD8372FE4B73327DB376D4FE5A111F,
	ManoEvents_add_OnCalibrationFailed_m296E3BA2759821A6EDDBB0C4AFA5DF5848099973,
	ManoEvents_remove_OnCalibrationFailed_m08B0FB631121276F33203A9423F09EBEDD37F385,
	ManoEvents_Awake_mF28C95758639C4C27B5AAC9C9629F4B6A882414F,
	ManoEvents_Update_mE3996862A733EFC2F38427968BB7A8CDB337B1BF,
	ManoEvents_HandleManomotionMessages_m59019849405EE78F761BF9547FF8D5B559CD2161,
	ManoEvents_DisplayLogMessage_m33F1F3A00E4F86D8F24AD4CE3C871B2916FE2029,
	ManoEvents__ctor_mAED7582697012075494C177D278254F400FCE8AB,
	ManoUtils_add_OnOrientationChanged_m71F65888A40DAA518ABA5478B09E4248C716731C,
	ManoUtils_remove_OnOrientationChanged_mCA61BE79E4126E0147444F23789229548C677086,
	ManoUtils_get_Instance_mC1A572CAE9F0FCBA38A805290E16A8C70EEFB82B,
	ManoUtils_Awake_m7EA7EBDB4F6E9A7A18583245D8F9D608311BCC2A,
	ManoUtils_CalculateNewPosition_m16A3438C80A3BEEC428534A64EC01311F3B928D6,
	ManoUtils_AjustBorders_mA199A7F01B0BF20F16B34BC2739ED6CE1D148402,
	ManoUtils_CalculateRatio_m355AED863DC91E4C4AB07B7015DB5BBA0ACA5434,
	ManoUtils_CalculateSize_m26A93B1043DD4AF3AF969F7CBEAF8F1615B3CF72,
	ManoUtils_AdjustMeshScale_m07D04D7B2E309572BC30845DEE37464A6AE1C2EA,
	ManoUtils_CalculateCorrectionPoint_mB1A40C0EE74C289AC2CB4C991AF164C41BCA5413,
	ManoUtils_Start_m7FFF76891004F54DFF42A191B288A199120D75B0,
	ManoUtils_Update_mF7B5AD9BA65ABEAC0561F658EC352B619742FA3B,
	ManoUtils_CheckForScreenOrientationChange_m8FC8ED5EEA35AFBD8E2970CBBD3A1E712234A7BD,
	ManoUtils_VectorAbs_m768F3210E2F9DDD13376DA3D152797D0CEFB09F3,
	ManoUtils_OrientMeshRenderer_m7A106C5DB8100AD01EC53C2AA75091D1BD185B81,
	ManoUtils__ctor_m500DC9F1EEFC25C4001EA74297DEC0250CB97A93,
	ManoVisualization_get_Show_inner_m564D482056E1A8CF38EB478487AFA01D38D11B73,
	ManoVisualization_set_Show_inner_m355C719D3C53BCD9659F942C2ED0CFF9FA4505F9,
	ManoVisualization_get_Show_fingertips_m04DEED3C830B0D2DF8A1D2C2F54EF61FA286DA25,
	ManoVisualization_set_Show_fingertips_m7EDDABC8352F2758E35B9E2987B0C696698DB69B,
	ManoVisualization_get_Show_fingertip_labels_m8CC4611CAB8812CB7EF50A79471DE447BA171532,
	ManoVisualization_set_Show_fingertip_labels_m86C64E2CB33B84E3E9CD1E7059CFA62E3682027D,
	ManoVisualization_get_Show_palm_center_mD4A7255DEA3BACCAC0C49121AF06C7C171EF0317,
	ManoVisualization_set_Show_palm_center_m111ADB07F9B74FA750103642D9A34BBF38FCF1EB,
	ManoVisualization_get_Show_contour_m3A4FE8BFDC4981C831CF6C19FD8F8D92E3C75902,
	ManoVisualization_set_Show_contour_m0CB85D6472F71EFA702CF7A6597D2EFF991452EB,
	ManoVisualization_get_Show_hand_layer_mBC951EA187A7D93588F22CCB792499F2DAB35C55,
	ManoVisualization_set_Show_hand_layer_m2447158D8D5A3FCF9ED98FEA4002B47778C7CC06,
	ManoVisualization_get_Show_background_layer_mB02DF8B958BAD185662A1CFE50AFD85CBB222FC3,
	ManoVisualization_set_Show_background_layer_m5FFFD38F5F8F2DD329639B564FDB66273E5A6429,
	ManoVisualization_get_Show_bounding_box_m3634CC0161588C79BB3247EEC3169D79D7AA31D8,
	ManoVisualization_set_Show_bounding_box_m2C7E4AD53CC8879BAA9A7B793DFFF7C27796B168,
	ManoVisualization_get_Show_joints_m7B2ADCC5B1E5BF696D26EF9580BC2D58C6BF7692,
	ManoVisualization_set_Show_joints_mCE65A80054BE0AD0E2E02EB439B0A5C81015F10B,
	ManoVisualization_Start_mD1AA96355E0D2C8DD54DBAA56B3A9258417385E5,
	ManoVisualization_CreateBoundingBoxes_mE1A7DE649AE5A2125712C5CC6A5551955E12D92D,
	ManoVisualization_SetHandsSupportedByLicence_m59CF7E352903AD0E51EEF1DB990AA9BBB1E16520,
	ManoVisualization_SetPointsBasedOnHandNumber_mB840EEFC6269CF7C9C96C83BDDC46AEF61249DA9,
	ManoVisualization_CreatePalmCenterParticle_mA524EAAED9BC9D562D311F6DDD418EF81E5648BA,
	ManoVisualization_CreateFingerTipParticles_m846F57E24FDEDFBF9DA427D9168F4519E88B3BFC,
	ManoVisualization_CreateFingerTipLabelParticles_m3ED4184DE2706CED33F3BE0C2648C6B2DD316911,
	ManoVisualization_CreateContourParticles_m4946469CEB7449BF0A30DCAA0FDDF2D3C370185F,
	ManoVisualization_CreateInnerParticles_m09A67451FC0F42802D098EBBF6870F986D79BECF,
	ManoVisualization_Update_m3CDE29CCF897941292C553957760B7DC99EBB856,
	ManoVisualization_UpdateLineColor_mA12AB6455F21839DE3DA32DE57E3B6E7C72A3198,
	ManoVisualization_UpdatePalmColor_m7CBA39B4F80EDCD0EDCAC196CE8D5F751D0A0F49,
	ManoVisualization_ShowPalmCenter_m9865D1BBAC74A7FA059EC9C3E35173B44C12BF89,
	ManoVisualization_ShowBoundingBoxInfo_mBA47D136D3031B8514B3AC9675B606EB3490F024,
	ManoVisualization_ShowFingerTips_mCFFF62C0F8640B4E8179FB40828848CB55BDC4B5,
	ManoVisualization_ShowFingerTipLabels_mBB2DC685F1C36B76106E2AD52D28715454E36C9F,
	ManoVisualization_ShowContour_m0388A84E30AFF9F49E835D1BB2CB7319AFE293AD,
	ManoVisualization_ShowInnerPoints_mA060165F6EE15B6BBDFA71F0A5B811195938715D,
	ManoVisualization_HighLightWithColor_m5BA73861CEA5A9C6C40FDEC7E4955F2154919025,
	ManoVisualization_ShowLayering_mAB412E294CCA1006BA0FA00CBC9446F51BE47F35,
	ManoVisualization_ShowBackground_m102F799D27B4E9C02A4C42BAD953FE5B1FC54D24,
	ManoVisualization_FadeColorAfterDelay_m813566DFED2493F4AEA1B089ED98F3C195D7364D,
	ManoVisualization_ToggleObjectVisibility_m0BB9713A850A08439B72E92AA270417A72F8ADA6,
	ManoVisualization_InstantiateManomotionMeshes_m1776A1AAAADBA3FFB46327FEBF0D64AB3EF2726E,
	ManoVisualization_ToggleTwoHandSupport_m27FA87F8D56738B691892521AB1CC70662BB0A12,
	ManoVisualization__ctor_m969DC29B4158F94BC3A7E58EDBCCCEDD30D97CE9,
	ManomotionManager_get_Serial_key_mAC91B63D3EE4B0C3B1107FC36C78E93EF1D56DB5,
	ManomotionManager_set_Serial_key_mB3337B758AB3641FF662812A4C07A17224EA02CD,
	ManomotionManager_init_m82B33D136988FE1A8894754FB658F4D8B7E79DF7,
	ManomotionManager_processFrame_mD2FF6FA8BEEAD742CF05DE0FB03F5534AC3DAACD,
	ManomotionManager_calibrate_m43DA61AD6480E97EA36BFEA96231C7BDD653438B,
	ManomotionManager_setFrameArray_m3F1EB20C26C5F98D5EB27A60723096967F617DB4,
	ManomotionManager_setMRFrameArray_mF83D2834630353DA8473449D9C546A570AA471D4,
	ManomotionManager_setResolution_mFC7B3679A8D26B2F5D7E78C67C5297FEACEA1972,
	ManomotionManager_setImageBinary_m0140D81D43286771E47B3E075D1470BC87BD82BC,
	ManomotionManager_get_Processing_time_mF16313A60B4D6683577E0BE539848F6D1171738E,
	ManomotionManager_get_Fps_m173B41D7167AE97992B19CC62788BF5B2AE02ABB,
	ManomotionManager_get_Height_m27B377FEE68F1E64094963856B88577323FAE75D,
	ManomotionManager_get_Width_m1F664657C3C0F81DDBE3D1D3FE59A000C82A50B5,
	ManomotionManager_get_Frame_number_m931EDF938311FB95D5A4778DEC8C5CD19043FD94,
	ManomotionManager_get_Visualization_info_m72DA1F8668736DEE8C728F616F7C341899356BFB,
	ManomotionManager_get_Hand_infos_m274F74E85F89F2B7B7D919C310796653CF88E741,
	ManomotionManager_get_Manomotion_Session_m13AB75678A9298A632A39A142B6D1C85965CED86,
	ManomotionManager_set_Manomotion_Session_m6C42E5075DF679E71F74F9662C7EDCA840A19486,
	ManomotionManager_get_Instance_m16B8789855D612EA23037A8C69012E212770F0C4,
	ManomotionManager_SetSelectedHand_m0A1ED553683B3736A734CDCCE337B96D9C8EE4D9,
	ManomotionManager_Awake_m39843CEF82BA098F1F0DC06F115B3C45B892AB6F,
	ManomotionManager_Start_mC688DE0C5A2156A2F769A0E2123A1A4832830D9C,
	ManomotionManager_SetCalibration_mEE3856612506F2C703D2D0A1501B9E6EA88019BF,
	ManomotionManager_StartWebCamTexture_mB7A06FD889157954AD1B737829394660F980ABD9,
	ManomotionManager_PickResolution_mD331224269080FEEF7D4BD2F21023F444611869C,
	ManomotionManager_InstantiateSession_m049A653271CC616614B1D0DCA02761215D97157C,
	ManomotionManager_InstantiateHandInfos_mA7D16C9F974CABD91A9151C625813942AB7A98FA,
	ManomotionManager_InstantiateVisualisationInfo_mEB689049944FEBC475134B4DA5E7B9F62CACD9C6,
	ManomotionManager_InitiateTextures_mEB15982F8AB492229A9013FB6F128DD725A60776,
	ManomotionManager_InitiateLibrary_mD459C001D7269D2E75364B3951E02356C6C7D7BB,
	ManomotionManager_SetVariables_mCBB745412CD7F29BB2692031EDB3F8F904D82EF0,
	ManomotionManager_SetHandInfo_m7F9E0FAFD77B15F6F04065DAB6D7CEEFBFD53650,
	ManomotionManager_SetVisualizationInfo_m043332763932051CFAF4EA8BFD7D645E2E64E7E0,
	ManomotionManager_SetManoManagerVariables_mF20A9EE81D2A602BA9BD0C5603CB35EA904DFB10,
	ManomotionManager_SetUnityConditions_m65AC86829C300B45B8C1267C7844D6C1E093652F,
	ManomotionManager_Init_m6208887BF1B1CA9301706F08FBC6547DD8755452,
	ManomotionManager_SetImageBinaries_mF130A6F2F777C1054DD2FDFF2F991BCD6683A3B5,
	ManomotionManager_SetResolution_mCA34F57D9544B6B18326F87A43676A64820978A0,
	ManomotionManager_SetFrameArray_mD9C00BE9C35B0083786D29FE4889BB3E8A2946BC,
	ManomotionManager_SetMRFrameArray_m06573C2418204E69AA334FE36D6F1DC1FC989E61,
	ManomotionManager_Update_m906D4CE09BAA84D972262DA8B56249280101D0A0,
	ManomotionManager_UpdateOrientation_m199B0D2B3837E3B709833A1CDEC512AB471D0743,
	ManomotionManager_UpdateTexturesWithNewInfo_m7519E593EDE352A13B71FC462C2B04817DA4BCFF,
	ManomotionManager_Set2HandSupport_mA15763327707A7EFCFDFD5EB39F44C5F0FFBF2FA,
	ManomotionManager_SetBackgroundMode_m812EE2507A39EB4181653960E82A14ECBAFB5776,
	ManomotionManager_UpdatePixelValues_m33F95BB8F25DDD3926B6526CFD2EF77FCD393142,
	ManomotionManager_ProcessManomotion_mE61D40D512BABB97DB00B10CCDA8FAE3B5A01CA4,
	ManomotionManager_CalculateFPSAndProcessingTime_m78554B35E2305B1B1B3DF44CD32CB80442CBC6B7,
	ManomotionManager_CalculateProcessingTime_m50DDD46A2267E2285A8062F3B48449F5A98798C3,
	ManomotionManager_ProcessFrame_m89BC645D41A7064201B06D8A1D519AFF87DB2F58,
	ManomotionManager_Calibrate_mEDA78D87AAC67AC1F9EB58F12FA8F066D30B6BA1,
	ManomotionManager__ctor_m26AD6DA6717A76B9B91D9F336342A3C1BA7114BF,
	ManomotionUIManagment_Start_mB16EEC83783B55FBC8D820F9E39028B8DBA15A59,
	ManomotionUIManagment_Update_m6B9B0FFFD00B5F5EFB4DAFC267E5331193D93D7B,
	ManomotionUIManagment_ToggleUIElement_m6300FB9BF4EFEC09A03E1FE2EA683EDB640BFFFB,
	ManomotionUIManagment_UpdateFPSText_mB75484D4557080B335D882808A1ADA21D934F274,
	ManomotionUIManagment_UpdateProcessingTime_mF5E2A1765A729BAEC5B05E2E2B935B77AD3DDBB5,
	ManomotionUIManagment_HideHandButtons_m7CFEABE49349A506C2D927863C82E20BBB4D16DD,
	ManomotionUIManagment_ShowHandButtons_m5F92BC43CBB717E645DA783DA5C394290A695EC1,
	ManomotionUIManagment__ctor_m79A86923B7BFA05BAF4E2E395086DF1A4DB87ED2,
	MenuManager_get_MenuIsOpen_m2E657B2CC8985DCA098182132F1432A7D8AB45E5,
	MenuManager_set_MenuIsOpen_m3D230C8ABC29B86AFE0EF1B5A7A92FEC8CE89AEB,
	MenuManager_Start_m253720A589CA2ECFD9BEECCEF10D8D0D0E1967F3,
	MenuManager_ToggleIconsMenu_m87517B43730D1D08A49534DC41BF3686844C7BD4,
	MenuManager_CloseMenu_mEAEBF1C7DA3AC8DE76CB08EA49363879C2DC1019,
	MenuManager__ctor_mF9508A23E828387B76DB818A2E34D3A8AC9063EF,
	ToggleGizmos_Start_m6EF259CDB9BC897C98D4ED60BF5B33127AB5D524,
	ToggleGizmos_ToggleShowRotation_mAEAC05E16E8E31E2CE209D73675452406ABB9007,
	ToggleGizmos_ToggleShowDepth_mC2BBFBDF463D940078B332EBCFFC033D8F19D9D1,
	ToggleGizmos_ToggleShowHandStates_mB19799A41198FBDCA7D5562B24B0E5DEAC49FE76,
	ToggleGizmos_ToggleShowManoclass_m069ED58FF81A5093ADD47DED0C76451B3A729A4B,
	ToggleGizmos_ToggleShowTriggerGesture_m50321C789A2F7F27A4AA716CB07855564D32283B,
	ToggleGizmos_ToggleShowContinuousGesture_mC085BAA9B0C2FED0E9DCEA919C6FC25D01E3A4FB,
	ToggleGizmos_ToggleBoundingBox_m42EB3F2F6811ED44C82098C567FD0B713B1F2E23,
	ToggleGizmos_ToggleShowFlags_mA26E955E16F9C5D8756AC99D2A9F7A3B9A31AD56,
	ToggleGizmos__ctor_m29BD44E68C1448C53603AEF5D2CFA2153481C7B9,
	ToggleVisualizationValues_Start_mCE8F24893A402C2B767C2D1BFE38FD538E1F6F0B,
	ToggleVisualizationValues_ToggleShowInner_m66910BF2ACF4D40EA9DB13AA2D64843E1478A5A0,
	ToggleVisualizationValues_ToggleShowContour_m6CC17D51B273DB1AD9292F81243B09C49C3B83D1,
	ToggleVisualizationValues_ToggleShowPalmCenter_mC8DF4EA4280BEBECC542DAB5600E95ED6AAEFDAD,
	ToggleVisualizationValues_ToggleShowFingertips_m7527EA86AB22964D534D357322F7303290A61218,
	ToggleVisualizationValues_ToggleShowFingertipLabels_m1043E3A83AB8E15B325384D5A08CE9AC9FBBE43E,
	ToggleVisualizationValues_ToggleShowHandLayer_m79BB727F252131D1604FB73593A7AF16E8CDA439,
	ToggleVisualizationValues_ToggleShowBackgroundLayer_mE90739FC3952B0075ADFFA5A509C5DDD47336473,
	ToggleVisualizationValues_ToggleBoundingBox_mD2E580ABB1BF6F2D9A9799CCAA6BA8899C51D9A0,
	ToggleVisualizationValues__ctor_mDF980F01D04103EE5E9F76E7511CCD07004CCC12,
	TwoHandSupport_Update_m806FE846407D2E0B3585929BFFD7BB3E83DFB3D0,
	TwoHandSupport__ctor_m0A341DE9C2CC94A3442CE7B499798378AF8F1DE4,
	UIIconBehavior_Start_m92DFD47A95DD088893833B2C2A5D97ACC3DF39C8,
	UIIconBehavior_UpdateIconAndFrame_m8D62BF99124B26B57184F4821A6CA9639DB373DE,
	UIIconBehavior_ToggleActive_m71245B86C09385A6960B4F56AC3CFB7DD712A391,
	UIIconBehavior_DeactivateIcon_m65FD6202060FDDB1B02DB05BE2D3195DBCEE4A77,
	UIIconBehavior__ctor_m008D18128F400BA12BDD5937A2F11299186DF1B9,
	UIRotationBehaviour_add_OnMenuEnabled_m24E9F3217FD401B121E7C4FD4076AF0AEABE2B17,
	UIRotationBehaviour_remove_OnMenuEnabled_m98FB9E8D89A9AD892EC7D00ED4B055ACEE8DC9BD,
	UIRotationBehaviour_OnEnable_mADBC5C9E498B1876297C176D5C8AC9FAB8EA604D,
	UIRotationBehaviour__ctor_m077BCE313BC9787B9C9DAE8150022B9D98CAB1E1,
	appearOnEnable_OnEnable_mA138307841C893900D280B4F5538352107F8649B,
	appearOnEnable__ctor_mF7337B725D054D41CEFE52D12DAFF0AFC248B650,
	BWEffect_Awake_mBC996F2191595DF4A18AE93014CC90D946E93EF6,
	BWEffect_OnRenderImage_mDD7CA2A3613B17F46B599FEDFF37656882671249,
	BWEffect__ctor_mBB8B592FB1FE69C7E1893BFCE2E941ABE8E016BB,
	GrabPass_Awake_m3E8E65B6374F3CFCF2773781F4DA01D550D9D6CC,
	GrabPass_Update_mD1CC7CB04FC67A2755204B0956FF98420169D625,
	GrabPass__ctor_m9D45212AB7B3CC10ADA32F622751A2C3FB3FFBF9,
	Grayscale_Awake_m7F697CE268A4589F84FA37033A5D011F999A94BD,
	Grayscale_OnRenderImage_m74CACBDDC52F0C488DCEF06E5216C66CEF89BC16,
	Grayscale__ctor_m68C4145AEFE0835F1A7EAC46B1D8639EF077B5CF,
	U3CCalculateU3Ed__19__ctor_m615A120FE639686915DE54EE3BA56E3FBD5AE822,
	U3CCalculateU3Ed__19_System_IDisposable_Dispose_m1D420E592E255D0E785A8EF71B75A3855576C129,
	U3CCalculateU3Ed__19_MoveNext_mC2B6BE17903B2D10CE286E863C5E46C7E4AB4D67,
	U3CCalculateU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m364515E3E7A8648D08860567D8798AFC1E093829,
	U3CCalculateU3Ed__19_System_Collections_IEnumerator_Reset_m6B702192F6C67CA73EC6DB10D2E0C88C5AA8068E,
	U3CCalculateU3Ed__19_System_Collections_IEnumerator_get_Current_m36978B32662276698B16DEE1EF99480FEE454CDD,
	U3CAlignIconsU3Ed__20__ctor_m82E26C81F63C7C8FA66A705447515BC322380D93,
	U3CAlignIconsU3Ed__20_System_IDisposable_Dispose_mB035ED02E82EC493AB05589CD8C88A0E74CD734E,
	U3CAlignIconsU3Ed__20_MoveNext_m3E284721779CFB785F294CC73AC1493D3D72ADD3,
	U3CAlignIconsU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDC8A47A814C55CAB1BB8A3D82452F0BF2D88EBA,
	U3CAlignIconsU3Ed__20_System_Collections_IEnumerator_Reset_m1815BB7ACAA4C9A6CEF1157C3D1577140A084DBC,
	U3CAlignIconsU3Ed__20_System_Collections_IEnumerator_get_Current_mAF6B74762DAB34C74D41465CDE77C3B06411350B,
	U3CPositionCategoriesAfterU3Ed__10__ctor_mADFE7016D30E1B41DBFAC253035F4AC67D2EED52,
	U3CPositionCategoriesAfterU3Ed__10_System_IDisposable_Dispose_m8044732A5DAE6BAB651F5206913AF230E94E9611,
	U3CPositionCategoriesAfterU3Ed__10_MoveNext_m489DE25363FA0B6A64269123AB81636455F4E5D0,
	U3CPositionCategoriesAfterU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58029AD776E76779D9AD2255A0F4C71A906D326F,
	U3CPositionCategoriesAfterU3Ed__10_System_Collections_IEnumerator_Reset_m4EA2A9A62FB45B05862DB6600D6D27A5192A616B,
	U3CPositionCategoriesAfterU3Ed__10_System_Collections_IEnumerator_get_Current_mE691FC30566BE55EB8AC42B74F1E1F6D9F085C13,
	U3CCloseAvailableBackgroundMenuAfterU3Ed__11__ctor_m92C7B4A24C6A55BC32663B7E5390B77CA1B75C46,
	U3CCloseAvailableBackgroundMenuAfterU3Ed__11_System_IDisposable_Dispose_mC1F66B8A3AA04EE3C373F799590C00FE4BBF391D,
	U3CCloseAvailableBackgroundMenuAfterU3Ed__11_MoveNext_m938942CD9CBB21D249CD2F022C022D0EBADAD0B0,
	U3CCloseAvailableBackgroundMenuAfterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46345104AB7BD9EA9A8B9979C16E973BFA99232F,
	U3CCloseAvailableBackgroundMenuAfterU3Ed__11_System_Collections_IEnumerator_Reset_m6A38DB620251104181527F64375443A8EDAFAD2E,
	U3CCloseAvailableBackgroundMenuAfterU3Ed__11_System_Collections_IEnumerator_get_Current_m889A2A5466E4CF12ED26F2A08EA1DCDF855C4D01,
	U3CCalibrateU3Ed__20__ctor_m93DCB7494AF7160C6A0285AE4644BAA8AA48D184,
	U3CCalibrateU3Ed__20_System_IDisposable_Dispose_m7E9602E82155B514D0BABF8572E33EAE4739B4CD,
	U3CCalibrateU3Ed__20_MoveNext_m1A2C5252919F19BCE26D847FB532C4DAB6FCABAE,
	U3CCalibrateU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05BEBAD7B3A9F6E4D36C979E17BD37955AD6BE58,
	U3CCalibrateU3Ed__20_System_Collections_IEnumerator_Reset_m8655E78450BEB05E3A541FEB57751F6F0942D74B,
	U3CCalibrateU3Ed__20_System_Collections_IEnumerator_get_Current_mF6801B748FE673463CAF253E3B1E8DE825A53115,
	ManoEvent__ctor_mC87803C22932E257AC870C00BB24E867C538024A,
	ManoEvent_Invoke_m151FAD05D37CDF2B3ADEDDACD8DDBFD7D1C16352,
	ManoEvent_BeginInvoke_m578ADA3488A4F864241560632FC8D017E75C07DB,
	ManoEvent_EndInvoke_m4755FDB3C8BB55FBAB8F634F5D538F9596C6FDB9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	OrientationChange__ctor_m37AFD52A8B8C6E36F81B889C33012541FE4F98A3,
	OrientationChange_Invoke_m99EC564D0A365E26B20602830313CA359A2CC8E0,
	OrientationChange_BeginInvoke_m8E446690B43886B7ED131292713922588B82A26E,
	OrientationChange_EndInvoke_mCC4375836A62027607889771E3EF7268D15154A0,
	U3CFadeColorAfterDelayU3Ed__101__ctor_m311B303671CC42F81CCD7C87DF5DF4D8F6F5B2C1,
	U3CFadeColorAfterDelayU3Ed__101_System_IDisposable_Dispose_m1F89791A19770A456830A2B4C7000B3F720CECE8,
	U3CFadeColorAfterDelayU3Ed__101_MoveNext_m747EB635C916D60FBDC70D3DECBB6753AC2815FC,
	U3CFadeColorAfterDelayU3Ed__101_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EB557C8672AF496D303ACF2DC46C9275B26D2A7,
	U3CFadeColorAfterDelayU3Ed__101_System_Collections_IEnumerator_Reset_mA85495DB7CC52E97315591C1B97C60CCAC450809,
	U3CFadeColorAfterDelayU3Ed__101_System_Collections_IEnumerator_get_Current_mF1360C8B3539659C517D847824CA6A9ABC43B446,
	MenuEnabled__ctor_m90B53FF12A4F3A207C95F2EC8F3B477B12F0D34D,
	MenuEnabled_Invoke_m9D222AE00791440547FC3224AE97C5365B519589,
	MenuEnabled_BeginInvoke_mD22C9B0D4B64F729459423498186D8AC62927F5B,
	MenuEnabled_EndInvoke_mB44125F61BD08551A8DD67B332EB6AD90FFE4ACB,
};
static const int32_t s_InvokerIndices[341] = 
{
	19,
	30,
	13,
	13,
	13,
	13,
	13,
	19,
	30,
	13,
	13,
	13,
	9,
	13,
	13,
	13,
	9,
	44,
	13,
	13,
	13,
	44,
	13,
	13,
	13,
	13,
	13,
	13,
	1250,
	13,
	13,
	13,
	13,
	13,
	1476,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	14,
	14,
	13,
	19,
	30,
	13,
	13,
	13,
	1367,
	13,
	19,
	13,
	13,
	13,
	14,
	13,
	19,
	30,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	13,
	13,
	9,
	1477,
	1478,
	1478,
	1478,
	1478,
	1477,
	9,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	19,
	30,
	13,
	13,
	13,
	13,
	13,
	1367,
	13,
	19,
	30,
	30,
	30,
	30,
	30,
	13,
	13,
	13,
	4,
	13,
	30,
	30,
	19,
	13,
	1479,
	1480,
	1481,
	1482,
	1483,
	1483,
	13,
	13,
	13,
	1076,
	4,
	13,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	17,
	44,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	1484,
	1484,
	1485,
	1485,
	1485,
	1485,
	1485,
	1485,
	1486,
	1487,
	23,
	1488,
	4,
	13,
	13,
	13,
	14,
	4,
	30,
	847,
	8,
	30,
	156,
	163,
	30,
	18,
	18,
	18,
	18,
	18,
	1489,
	14,
	1490,
	1491,
	19,
	9,
	13,
	13,
	9,
	13,
	149,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	149,
	4,
	13,
	13,
	13,
	13,
	9,
	9,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	13,
	17,
	44,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	44,
	13,
	13,
	13,
	30,
	30,
	13,
	13,
	13,
	13,
	13,
	23,
	13,
	13,
	13,
	13,
	13,
	23,
	13,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	144,
	13,
	16,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	144,
	13,
	16,
	4,
	9,
	13,
	17,
	14,
	13,
	14,
	144,
	13,
	16,
	4,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	341,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
